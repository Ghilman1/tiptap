﻿using UnityEngine;
using System.Collections;

public class Player_ZigZag : MonoBehaviour {


	//bool right;
	public static bool right;
    public static float speed = 1.5f;
	//public static float speed = 9.0f;
	bool camRotateDone;
	//public static bool camRotateDone;
	public Transform camTrans;
	float angle = 0.0f;
	bool blockesIncreased;
	int turn = 0;
	bool canCamRotate = false;
//	public static bool canCamRotate = false;
	bool canGetTouch = true;
	Vector3 camStartPos;
	Vector3 camEndPos;
	Vector3 camStartAngle;
	Vector3 camEndAngle;
	float camMovePercentage = 0.0f;
	float camOffset = 0.0f;

    public static float sphearRotation=250;

	public static bool inputEnable=true;
	public static bool MagicEnable=true;


	public float turnDistance;
	public int distance;
	public static int notDiamondDistance;
	public static bool sphereCallBack;

	public float randNum;
	public float randTile;
	public static float prob;
	public GameObject newParent;
//	public static bool isRestart=false;



	void Start () {
		prob = 0.2f;
		speed = 1.5f;
	//	speed = 7.0f;
		sphearRotation=500;
		//	GuiCalls.isXexis = false;
		//	GuiCalls.isZexis = false;
		SaveMeCalls.isXexis = false;
		SaveMeCalls.isZexis = false;

//		isRestart = false;
	//	if(GuiCalls.isXexis){right = false;}else{right = true;}
		right = true;
		camRotateDone = true;
		blockesIncreased = false;
		camOffset = camTrans.position.z - transform.position.z;
		camOffset = -camOffset;
		PlayerPrefs.SetInt ("distance",0);
		notDiamondDistance=0;
		PlayerPrefs.SetInt ("notDiamondDistance",0);

	}



	void Update () {


		RaycastHit hitInfo;

		if (!Physics.Raycast (transform.position, Vector3.down, out hitInfo, 6.0f)) {
		//	AdsController.showIntertial();
			camTrans.parent = newParent.transform;
			GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.None;
			GetComponent<Player_ZigZag> ().enabled = false;
			GameObject.Find ("Spawner").GetComponent<ZigZagTileSpawner>().enabled=false;
			////////
			//Debug.Log(Achievements1.getInstance().isNotColllectDiamnds);
			Achievements1.getInstance ().setAchievementsRolling();
			Achievements1.getInstance ().setAchievementsCollectGem();

			/////
			GameNguiUpdater.isGameEnd=true;
			GameObject.Find ("G_P_S").GetComponent<GooglePlayServices>().scoreUpdate();
		//	GooglePlayServices.isScoreUpdate=true;

		}
		if(GetMagic.MagicOn==false&&GetBallScale.ballScaleOn == false){
	//if (GetBallScale.ballScaleOn == false) {
			sphearRotation += Time.deltaTime * 7;
			PlayerPrefs.SetFloat ("onMagicSphearRotation",sphearRotation);
	//	}
		speed += (Time.deltaTime / 30);
			PlayerPrefs.SetFloat("onMagicSpeed",speed);

	   }
        
		if (speed > 3 && speed <= 4) {
			ZigZagTileSpawner.NoOfBlocks = 3;
		} else if (speed > 4 && speed <= 5) {
			ZigZagTileSpawner.NoOfBlocks = 4;	
		} else if (speed > 5 && speed <= 6) {
			ZigZagTileSpawner.NoOfBlocks = 4;	
		} else if (speed > 6 && speed <= 7) {
			ZigZagTileSpawner.NoOfBlocks = 5;	
		} else if (speed > 7 && speed <= 8) {
//			randNum = Random.Range (0.0f, 1.0f);
//			if(randNum>1-prob){
//				ZigZagTileSpawner.NoOfBlocks = 3;
//			}else{ZigZagTileSpawner.NoOfBlocks = 6;}
			ZigZagTileSpawner.NoOfBlocks = 6;
		} else if (speed > 8 && speed <= 8) {
			randNum = Random.Range (0.0f, 1.0f);
			if(randNum>1-0.1f){
				ZigZagTileSpawner.NoOfBlocks = 3;
			}else if(randNum>1-0.3f){
				ZigZagTileSpawner.NoOfBlocks = 4;
			}else if(randNum>1-0.4){
				ZigZagTileSpawner.NoOfBlocks = 5;
			}else{ZigZagTileSpawner.NoOfBlocks = 6;}

		} else if (speed > 9) {
			speed = 9.1f;
			sphearRotation= 2001f;

		} 


		turnDistance=turnDistance+(speed*Time.deltaTime);
			if(turnDistance>=1.0f)
			{
				turnDistance=0.0f;
			    distance=distance+1;
			    PlayerPrefs.SetInt ("distance",distance);
			notDiamondDistance=notDiamondDistance+1;
			PlayerPrefs.SetInt ("notDiamondDistance",notDiamondDistance);
//			Debug.Log(notDiamondDistance);
			}
//		}
		if (inputEnable && GetMagic.MagicOn==false) {
		
		bool pass = Input.GetKeyDown (KeyCode.A);
		if (Input.touchCount > 0 || pass) {
		
			if ((pass || Input.GetTouch (0).phase.Equals (TouchPhase.Began)) && canGetTouch) {
					transform.eulerAngles = new Vector3 (0, 0, 0);
				if (right) {

					right = false;

					if (Mathf.Abs (ZigZagTileSpawner.turnPoints [turn].z - transform.position.z) <= 0.50f && Mathf.Abs (ZigZagTileSpawner.turnPoints [turn].x - transform.position.x) <= 0.50f) {
						    turn++;
						    canCamRotate = true;
						    camRotateDone = false;
						    camStartPos = camTrans.position;
						    camStartAngle = camTrans.eulerAngles;
						    camEndPos = new Vector3 (transform.position.x + camOffset, camTrans.position.y, ZigZagTileSpawner.turnPoints [turn - 1].z);
						    camEndAngle = camTrans.eulerAngles;
						    camEndAngle.y = camTrans.eulerAngles.y - 90.0f;
						    canGetTouch = false;
						    GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY;
					        //GuiCalls.spherePos=transform.position;
						    //GuiCalls.isXexis=true;
					        //GuiCalls.isZexis=false;
					   //     SaveMeCalls.spherePos=transform.position;
							SaveMeCalls.isXexis=true;
							SaveMeCalls.isZexis=false;
							
					}
				} else {
					
						if ((Mathf.Abs (ZigZagTileSpawner.turnPoints [turn].x - transform.position.x) <= 0.50f && Mathf.Abs (ZigZagTileSpawner.turnPoints [turn].z - transform.position.z) <= 0.50f)) {
						turn++;
						
						//	transform.eulerAngles = new Vector3(0,0,0);
						    canCamRotate = true;
						    camRotateDone = false;
						    camStartPos = camTrans.position;
						    camStartAngle = camTrans.eulerAngles;
						    camEndPos = new Vector3 (ZigZagTileSpawner.turnPoints [turn - 1].x, camTrans.position.y, transform.position.z - camOffset);
						    camEndAngle = camTrans.eulerAngles;
						    camEndAngle.y = camTrans.eulerAngles.y + 90.0f;
						    canGetTouch = false;
					
						    GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY;
						    //GuiCalls.cameraPos=camTrans.position;
					        //GuiCalls.spherePos=transform.position;
					        //GuiCalls.isZexis=true;
						    //GuiCalls.isXexis=false;
						//	SaveMeCalls.spherePos=transform.position;
							SaveMeCalls.isZexis=true;
							SaveMeCalls.isXexis=false;
					}
					right = true;
				}
			}
		}

	}//inputEnable
		if (inputEnable == false && GetMagic.MagicOn) {
			               

			if (ZigZagTileSpawner.turnPoints [turn].z - transform.position.z <= 0.25f&& Mathf.Abs (ZigZagTileSpawner.turnPoints [turn].x - transform.position.x) <= 0.5f) {
				          
				if (right) {
					transform.eulerAngles = new Vector3 (0, 0, 0);
					turn++;
					canCamRotate = true;
					camRotateDone = false;
					camStartPos = camTrans.position;
					camStartAngle = camTrans.eulerAngles;
					camEndPos = new Vector3 (transform.position.x + camOffset, camTrans.position.y, ZigZagTileSpawner.turnPoints [turn - 1].z);
					camEndAngle = camTrans.eulerAngles;
					camEndAngle.y = camTrans.eulerAngles.y - 90.0f;
				//	SaveMeCalls.spherePos=transform.position;
					SaveMeCalls.isXexis=true;
					SaveMeCalls.isZexis=false;
					right = false;
				
				}


			}
			if (Mathf.Abs (ZigZagTileSpawner.turnPoints [turn].x - transform.position.x) <= 0.25f && Mathf.Abs (ZigZagTileSpawner.turnPoints [turn].z - transform.position.z) <= 0.5f) {
				if( right == false){
				
				    transform.eulerAngles = new Vector3 (0, 0, 0);

			
				    turn++;

				    canCamRotate = true;
				    camRotateDone = false;
				    camStartPos = camTrans.position;
				    camStartAngle = camTrans.eulerAngles;
				    camEndPos = new Vector3 (ZigZagTileSpawner.turnPoints [turn - 1].x, camTrans.position.y, transform.position.z - camOffset);
				    camEndAngle = camTrans.eulerAngles;
			        camEndAngle.y = camTrans.eulerAngles.y + 90.0f;
				//    SaveMeCalls.spherePos=transform.position;
					SaveMeCalls.isZexis=true;
					SaveMeCalls.isXexis=false;
					right = true;
			   
			}
					
			}
		
		} 




		if (right) {
			if(!camRotateDone && canCamRotate){
			
				camEndPos.z = transform.position.z-camOffset;
				//	if(GetMagic.MagicOn==true){camMovePercentage += 5*((speed/2)*Time.deltaTime);}else{camMovePercentage += 5*Time.deltaTime;}
				camMovePercentage += (5*Time.deltaTime);//*(speed/2.0f);//speed*some value
				camTrans.eulerAngles = Vector3.Lerp(camStartAngle,camEndAngle,camMovePercentage);
				camTrans.position = Vector3.Lerp(camStartPos,camEndPos,camMovePercentage);
				//GuiCalls.cameraPos=camTrans.position;
				SaveMeCalls.cameraPos=camTrans.position;
				SaveMeCalls.spherePos=transform.position;
				if(camMovePercentage >= 1.0){
				
					camRotateDone = true;
					canCamRotate = false;
					camMovePercentage = 0.0f;
					canGetTouch=true;

					
				}

			}else{
				camTrans.position += new Vector3(0,0,speed*Time.deltaTime);
			}

			transform.position += new Vector3(0,0,speed*Time.deltaTime);
		    this.transform.Rotate (1*Time.deltaTime*sphearRotation,0,0);

		} else {
			if(!camRotateDone && canCamRotate){
				camEndPos.x = transform.position.x+camOffset;
			//	if(GetMagic.MagicOn==true){camMovePercentage += 5*((speed/2)*Time.deltaTime);}else{camMovePercentage += 5*Time.deltaTime;}
				camMovePercentage += (5*Time.deltaTime);//*(speed/2.0f);
				camTrans.eulerAngles = Vector3.Lerp(camStartAngle,camEndAngle,camMovePercentage);
				camTrans.position = Vector3.Lerp(camStartPos,camEndPos,camMovePercentage);
			    //GuiCalls.cameraPos=camTrans.position;
				SaveMeCalls.cameraPos=camTrans.position;
		        SaveMeCalls.spherePos=transform.position;
				if(camMovePercentage >= 1.0){
					camRotateDone = true;
					canCamRotate = false;
					camMovePercentage = 0.0f;
					canGetTouch = true;
				
					
				}

			}else{
				camTrans.position += new Vector3(-speed*Time.deltaTime,0,0);

			}
			transform.position += new Vector3(-speed*Time.deltaTime,0,0);
			this.transform.Rotate (0,0,1*Time.deltaTime*sphearRotation);
		}
	}
}
