﻿using UnityEngine;
using System.Collections;
//using UnityEditor;

public class Tile : MonoBehaviour {

	public GameObject Dimond,TileLeftCorner,TileRightCorner,AlongZ_MountPref,AlongX_MountPref;
	public Vector3 offset;
	public Vector3 Tempoffset;
	public Vector3 spaceBtweenCoins;
	public GameObject clone,Corner_Clone,AlonX_Mount_clone,AlonZ_Mount_clone;
	public int unit=2;
	public int speed=2;
    public Renderer rend;

	public float ScaleToTiles= 0.667f;
	public int mountain_count=0;
	// Use this for initialization
	GameObject sphere;
	bool destroyed = false;
	void Start () {

		transform.parent = GameObject.Find("TilesParent").transform;
		rend = GetComponent<Renderer>();
		sphere = GameObject.Find ("Sphere"); 


	//	Dimond Creation Start
		int rand = Random.Range (0,3);
		int r_Mount_Rand= Random.Range (0,2);
		int l_Mount_Rand= Random.Range (0,2);
		                
						if (transform.localScale.x > 1) {
		//	mountain_count++;
			Vector3 tileCor_Pos=new Vector3(transform.position.x+((transform.localScale.x/2)-0.5f),transform.position.y+(transform.localScale.y/2)+0.001f,transform.position.z);
			Corner_Clone = Instantiate (TileLeftCorner, tileCor_Pos, Quaternion.identity) as GameObject;
			Corner_Clone.transform.Rotate (90,-180,0);
			Corner_Clone.transform.parent=this.gameObject.transform;

		
			        
						for (int i=1; i<=rand; i++) {
			
								Vector3 diamond_pos = new Vector3 (-(transform.localScale.x / rand) * i + transform.position.x + transform.localScale.x / 2, transform.position.y, transform.position.z);
								offset = diamond_pos;
								offset.y = diamond_pos.y + 0.7f;
								clone = Instantiate (Dimond, offset, Quaternion.identity) as GameObject;
				                clone.transform.parent=this.gameObject.transform;
				                  
						                              }
//			Environment_Maker.environmentMaker();
			Vector3 AlongX_Mount_pos=new Vector3(transform.position.x,transform.position.y,transform.position.z);
			AlonX_Mount_clone = Instantiate (AlongX_MountPref, AlongX_Mount_pos, Quaternion.identity) as GameObject;
			AlonX_Mount_clone.transform.GetChild (0).GetChild(l_Mount_Rand).gameObject.SetActive(true);
			AlonX_Mount_clone.transform.GetChild (1).GetChild(r_Mount_Rand).gameObject.SetActive(true);
			AlonX_Mount_clone.transform.parent=this.gameObject.transform;
			
			

				       } else if (transform.localScale.z > 1) {
			Vector3 tileCor_Pos=new Vector3(transform.position.x,transform.position.y+(transform.localScale.y/2)+0.001f,transform.position.z-((transform.localScale.z/2)-0.5f));
			Corner_Clone = Instantiate (TileRightCorner, tileCor_Pos, Quaternion.identity) as GameObject;
			Corner_Clone.transform.Rotate (90,0,0);
		    Corner_Clone.transform.parent=this.gameObject.transform;

	
						for (int i=1; i<=rand; i++) {

								Vector3 diamond_pos = new Vector3 (transform.position.x, transform.position.y,(transform.localScale.z / rand) * i + transform.position.z - transform.localScale.z / 2);
				                offset = diamond_pos;
				                offset.y = diamond_pos.y + 0.7f;
				                clone = Instantiate (Dimond, offset, Quaternion.identity) as GameObject;
			                 	clone.transform.parent=this.gameObject.transform;
				                //clone. transform.Rotate(0, Input.GetAxis("Horizontal")*speed*Time.deltaTime, 0,Space.World);

						                            }
			Vector3 AlongZ_Mount_pos=new Vector3(transform.position.x,transform.position.y,transform.position.z);
	//		AlongZ_MountPref.transform.localScale=new Vector3(1/(transform.localScale.z),1/(transform.localScale.z),1/(transform.localScale.z));
//			AlongZ_MountPref.transform.GetChild (0).transform.localScale=new Vector3(1/(9-transform.localScale.z),1/(9-transform.localScale.z),1/(9-transform.localScale.z));
	//	AlongZ_MountPref.transform.GetChild (1).transform.localScale=new Vector3(1/(9-transform.localScale.z),1/(9-transform.localScale.z),1/(9-transform.localScale.z));
	//	AlongZ_MountPref.transform.GetChild (2).transform.localScale=new Vector3(1/(9-transform.localScale.z),1/(9-transform.localScale.z),1/(9-transform.localScale.z));
		//	AlonZ_Mount_clone = Instantiate (AlongZ_MountPref, AlongZ_Mount_pos, Quaternion.identity) as GameObject;
			//Corner_Clone.transform.parent=this.gameObject.transform;
				                                       }
		//	Dimond Creation end
	
				
	}

	void Update(){



		if (sphere.transform.position.x - transform.position.x < -5.0f && sphere.transform.position.z 
		    - transform.position.z > 5.0f && !destroyed) {
			GameObject.Find("Spawner").GetComponent<ZigZagTileSpawner>().SpawnNewTile();
			destroyed = true;
			Destroy(this.gameObject,1.0f);
		}
	}
}
