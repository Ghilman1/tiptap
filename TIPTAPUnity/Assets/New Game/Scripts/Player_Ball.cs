﻿using UnityEngine;
using System.Collections;

public class Player_Ball : MonoBehaviour {



	int dir;
	int x = 1;
	int z = 2;
	int currentDir;
	float speed = 0.5f;
	void Start () {
		dir = z;
		currentDir = z;
		GetNextDir ();
	}
	

	void Update () {
		if (Input.GetKeyDown(KeyCode.A)) {
			currentDir = dir;
			GetNextDir();
			Debug.Log(dir);
			Debug.Log("A");
		}
		if (currentDir == z) {
			transform.position += new Vector3 (0, 0, speed * Time.deltaTime);
		} else if (currentDir == -z) {
			transform.position += new Vector3 (0, 0, -speed * Time.deltaTime);
		}else if(currentDir == x){
			transform.position += new Vector3 (speed * Time.deltaTime, 0, 0);
		}else if(currentDir == -x){
			transform.position += new Vector3 (-speed * Time.deltaTime, 0, 0);
		}
	}

	void GetNextDir(){
		for(int i = 1;i<TileSpawner.MaxLength;i++){
			if(dir == z){
				Vector3 temp1 = new Vector3(transform.position.x -1,0,transform.position.z + i);
				Vector3 temp2 = new Vector3(transform.position.x +1,0,transform.position.z + i);
				foreach(Collider col in TileSpawner.tiles){
					if(col.bounds.Contains(temp1)){
						dir = -x;
						return;
					}
					if(col.bounds.Contains(temp2)){
						dir = x;
						return;
					}
				}
			}else if(dir == -z){
				Vector3 temp1 = new Vector3(transform.position.x -1,0,transform.position.z - i);
				Vector3 temp2 = new Vector3(transform.position.x +1,0,transform.position.z - i);
				foreach(Collider col in TileSpawner.tiles){
					if(col.bounds.Contains(temp1)){
						dir = -x;
						return;
					}
					if(col.bounds.Contains(temp2)){
						dir = x;
						return;
					}
				}
			}else if(dir == x){
				Vector3 temp1 = new Vector3(transform.position.x +i,0,transform.position.z + 1);
				Vector3 temp2 = new Vector3(transform.position.x +i,0,transform.position.z - 1);
				foreach(Collider col in TileSpawner.tiles){
					if(col.bounds.Contains(temp1)){
						dir = z;
						return;
					}
					if(col.bounds.Contains(temp2)){
						dir = -z;
						return;
					}
				}
			}else if(dir == -x){
				Vector3 temp1 = new Vector3(transform.position.x - i,0,transform.position.z + 1);
				Vector3 temp2 = new Vector3(transform.position.x + i,0,transform.position.z - 1);
				foreach(Collider col in TileSpawner.tiles){
					if(col.bounds.Contains(temp1)){
						dir = z;
						return;
					}
					if(col.bounds.Contains(temp2)){
						dir = -z;
						return;
					}
				}
			}
		}
	}
}
