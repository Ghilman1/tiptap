﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileSpawner : MonoBehaviour {

	public GameObject Tile;
	public Vector3 nextTilePos;
	bool left = false;
	bool right = false;
	int Tiles_To_Spawn = 20;
	public Transform Spawner;

	int sameTurn = 1;

	int dir = 1;//0 for +x , 1 for -x,2 for +z,3 for -z
	int x = 1;
	int z = 2;
	public static int MaxLength = 5;
	public static List<Collider> tiles;
	void Awake () {
		left = true;
		nextTilePos = new Vector3(Spawner.position.x - Spawner.localScale.x+0.5f,0,
		                          Spawner.position.z);
		dir = -x;
		tiles = new List<Collider> ();
		tiles.Add (this.gameObject.GetComponent<Collider>());
		SpawnTiles ();
	}

	void Update () {
		

	}

	void SpawnTiles(){
		for(int i = 0;i<Tiles_To_Spawn;i++){
			if(Random.Range(0,2)==1){
				if(left){
					sameTurn++;
					if(sameTurn >2){
						left = false;
						sameTurn = 1;
					}
				}else{
					left = true;
					sameTurn = 1;
				}

			}else{
				if(!left){
					sameTurn++;
					if(sameTurn >2){
						left = true;
						sameTurn = 1;
					}
				}else{
					left = false;
					sameTurn = 1;
				}

			}
			int length = Random.Range(2,MaxLength);
			Debug.Log(length);
			Tile.transform.localScale = new Vector3(length,0.2f,1);

			if(dir == -x){
				if(left){
					nextTilePos.z = nextTilePos.z - (float)length/2+.5f;
					dir = -z;
					GameObject go = Instantiate(Tile,nextTilePos,Quaternion.identity) as GameObject;
					go.transform.Rotate(0,90,0);
					tiles.Add(go.GetComponent<Collider>());
				}else{
					nextTilePos.z = nextTilePos.z + (float)length/2-.5f;
					dir = z;
					GameObject go = Instantiate(Tile,nextTilePos,Quaternion.identity) as GameObject;
					go.transform.Rotate(0,90,0);
					tiles.Add(go.GetComponent<Collider>());
				}
			}else if(dir == x){
				if(left){
					nextTilePos.z = nextTilePos.z + (float)length/2-.5f;
					dir = z;
					GameObject go = Instantiate(Tile,nextTilePos,Quaternion.identity) as GameObject;
					go.transform.Rotate(0,90,0);
					tiles.Add(go.GetComponent<Collider>());
				}else{
					nextTilePos.z = nextTilePos.z - (float)length/2+.5f;
					dir = -z;
					GameObject go = Instantiate(Tile,nextTilePos,Quaternion.identity) as GameObject;
					go.transform.Rotate(0,90,0);
					tiles.Add(go.GetComponent<Collider>());
				}
			}else if(dir == z){
				if(left){
					nextTilePos.x = nextTilePos.x - (float)length/2+.5f;
					dir = -x;
					GameObject go = Instantiate(Tile,nextTilePos,Quaternion.identity) as GameObject;
					tiles.Add(go.GetComponent<Collider>());
				}else{
					nextTilePos.x = nextTilePos.x + (float)length/2-.5f;
					dir = x;
					GameObject go = Instantiate(Tile,nextTilePos,Quaternion.identity) as GameObject;
					tiles.Add(go.GetComponent<Collider>());
				}
			}
			else if(dir == -z){
				if(left){
					nextTilePos.x = nextTilePos.x + (float)length/2-.5f;
					dir = x;
					GameObject go = Instantiate(Tile,nextTilePos,Quaternion.identity) as GameObject;
					tiles.Add(go.GetComponent<Collider>());
				}else{
					nextTilePos.x = nextTilePos.x - (float)length/2+.5f;
					dir = -x;
					GameObject go = Instantiate(Tile,nextTilePos,Quaternion.identity) as GameObject;
					tiles.Add(go.GetComponent<Collider>());
				}
			}
//			Debug.Log(left);
//			Debug.Log(dir);

			if(dir == x){
				nextTilePos.x += length/2;
			}else if(dir == -x){
				nextTilePos.x -= length/2;
			}else if(dir == z){
				nextTilePos.z += length/2;
			}else if (dir == -z){
				nextTilePos.z -= length/2;
			}
		}
	}

}
