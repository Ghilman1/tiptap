﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class ZigZagTileSpawner : MonoBehaviour {

	public GameObject PowerUp1;
	public GameObject PowerUpClone;
	public GameObject PowerUp2;
	public GameObject PowerUp3;

	float tile_y;
	public  Vector3 nextTilePos;
	static bool leftSpawn;
	public GameObject tile;
	public static int NoOfBlocks;
	public int counter = 0;
	public static List<Vector3> turnPoints;

	public static int  tileCounterStart;
	public Vector3 offset;

	float randomValue;

	float randomPower;
	int randomPower1;
	int adsCont=0;
	int isPowerUpGenerated=0;
	public static float Prob;

	//public GameObject tile_go;

	void Start () {

		Prob = 0.2f;

	
		nextTilePos = new Vector3(transform.position.x - transform.localScale.x/2,0,
		                          transform.position.z);
		tile_y = nextTilePos.y;
		//tile_go.transform.position = nextTilePos;
		leftSpawn = false;
		NoOfBlocks = 2;
		turnPoints = new List<Vector3> ();

		GameObject go = GameObject.Find ("StartingFloor") as GameObject;

		turnPoints.Add (new Vector3(0.0f,0.75f,go.transform.position.z+0.5f+go.transform.localScale.z/2));
		SpawnNewTile ();
	}

	void Update () {
		if (counter < 20) {
			SpawnNewTile();
			counter++;
		

		}



	}

	public void SpawnNewTile(){
	

		randomPower = Random.Range (0.0f, 1.0f);
		randomPower1 = Random.Range (0, 5);

		tileCounterStart++;
		if (tileCounterStart > 20) {

			tileCounterStart=0;
				}

		//nextTilePos.y = tile_go.transform.position.y;
			//+ tile_go.transform.localScale.y / 2;
     	nextTilePos.y = tile_y;
		int len = Random.Range (NoOfBlocks, NoOfBlocks+6);

		float length = len * 1.0f;
		if (leftSpawn) {

			nextTilePos.z += 0.5f;
			turnPoints.Add (new Vector3(nextTilePos.x,nextTilePos.y+0.75f,nextTilePos.z));
			tile.transform.localScale = new Vector3(length,1.0f,1);
			nextTilePos.x -= length/2 - 0.5f;
			GameObject tile_go = Instantiate(tile,nextTilePos,Quaternion.identity) as GameObject;
		//	tile_go.transform.Rotate(0,0,10);
			leftSpawn = false;
			nextTilePos.x -= length/2;

			// Collectables 
//			if (tileCounterStart==15&&randomPower==2 && BallScale.BallScale_Look==false) {
//			    offset= nextTilePos;
//				offset.x= nextTilePos.x+2.0f;
//				offset.y=nextTilePos.y+0.9f;
//				PowerUpClone = Instantiate (PowerUp1, offset, Quaternion.identity) as GameObject;
//				tileCounterStart=0;
//			}else if (tileCounterStart==15&&randomPower==3 && Magic.magic_Look==false) {
//				offset= nextTilePos;
//				offset.x= nextTilePos.x+2.0f;
//				offset.y=nextTilePos.y+0.9f;
//				PowerUpClone = Instantiate (PowerUp2, offset, Quaternion.identity) as GameObject;
//				tileCounterStart=0;
//			}else if (tileCounterStart==15&&randomPower==4 && Magnet.magnet_Look==false) {
//				offset= nextTilePos;
//				offset.x= nextTilePos.x+2.0f;
//				offset.y=nextTilePos.y+0.9f;
//				PowerUpClone = Instantiate (PowerUp3, offset, Quaternion.identity) as GameObject;
//				tileCounterStart=0;
//			}
			// Collectables

		} else {

			nextTilePos.x -= 0.5f;
			turnPoints.Add (new Vector3(nextTilePos.x,nextTilePos.y+0.75f,nextTilePos.z));
			tile.transform.localScale = new Vector3(1,1.0f,length);
			nextTilePos.z += length/2 - 0.5f;
			GameObject tile_go = Instantiate(tile,nextTilePos,Quaternion.identity) as GameObject;
		//	tile_go.transform.Rotate(10,0,0);
			leftSpawn = true;
			nextTilePos.z += length/2;

			if(tileCounterStart > 10){
				if(randomPower > 1- Prob){
					if(Progression.powerUpsCounter() > 0){
						int rand_ = Random.Range(0,Progression.powerUpsCounter())+1;
						if(rand_==1&& Magnet.getInstance().isMagetLock==1){
							offset= nextTilePos;
							offset.y=nextTilePos.y+0.7f;
							offset.z=nextTilePos.z-1.5f;
							PowerUpClone = Instantiate (PowerUp1,offset, Quaternion.identity) as GameObject;
							tileCounterStart=0;
						}
						if(rand_==2&&Magic.getInstance().isMagicLock==1){
							offset= nextTilePos;
							offset.y=nextTilePos.y+0.7f;
							offset.z=nextTilePos.z-1.5f;
							PowerUpClone = Instantiate (PowerUp2,offset, Quaternion.identity) as GameObject;
							tileCounterStart=0;
						}
						if(rand_==3&& BallScale.getInstance().isBallScaleLock==1){
							offset= nextTilePos;
							offset.y=nextTilePos.y+0.7f;
							offset.z=nextTilePos.z-1.5f;
							PowerUpClone = Instantiate (PowerUp3,offset, Quaternion.identity) as GameObject;
							tileCounterStart=0;
						}
						if(Prob == 0.2f){
							Prob = 0.05f;
						}else{
							Prob = 0.01f;
						}
					}
				}

			}
			// Collectables
//			if (tileCounterStart==15 && randomPower1==2 && Magnet.getInstance().isMagetLock==1) {
//			
//				offset= nextTilePos;
//		    	offset.y=nextTilePos.y+0.9f;
//				offset.z=nextTilePos.z-1.5f;
//				PowerUpClone = Instantiate (PowerUp1,offset, Quaternion.identity) as GameObject;
//				tileCounterStart=0;
//			}
//			if (tileCounterStart==15&&randomPower1==3 && BallScale.getInstance().isBallScaleLock==1) {
//				
//				offset= nextTilePos;
//				offset.y=nextTilePos.y+0.9f;
//				offset.z=nextTilePos.z-1.5f;
//				PowerUpClone = Instantiate (PowerUp2,offset, Quaternion.identity) as GameObject;
//				tileCounterStart=0;
//			}
//			if (tileCounterStart==15&&randomPower1==4 && Magic.getInstance().isMagicLock==1) {
//				
//				offset= nextTilePos;
//				offset.y=nextTilePos.y+0.9f;
//				offset.z=nextTilePos.z-1.5f;
//				PowerUpClone = Instantiate (PowerUp3,offset, Quaternion.identity) as GameObject;
//				tileCounterStart=0;
//			}
			// Collectables
		}
	}
}
