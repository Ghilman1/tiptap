﻿using UnityEngine;
using System.Collections;

public class UpgradersNgui : MonoBehaviour {

	public string imageName="Bright";

	public   GameObject notEnoughRes;
	public static bool IsNotEnoughRes=false;
	//public static bool updateDiamonds=false;

	// Use this for initialization
	void Start () {
		//	updateDiamonds = true;
		//	updateDiamonds = false;
		ballScaleUplevelsUp();
		magicLevelsUp ();
		MagnetLevelsUp ();
		
		magicButtonLabel ();
		magnetButtonLabel();
		ballScaleUpButtonLabel ();
	
	}
	public void ballScaleUplevelsUp(){
		if( BallScale.getInstance().isBallScaleLock==1){
			for(int k=0;k<(BallScale.getInstance ().ballScale_Level);k++){
				GameObject.Find ("BallScaleUpLevel"+(k)).GetComponent<UISprite>().spriteName=imageName;
			}}}
	public void magicLevelsUp(){
		if(Magic.getInstance().isMagicLock==1){
			for(int i=0;i<(Magic.getInstance ().magic_Level);i++){
				GameObject.Find ("MagicLevel"+(i)).GetComponent<UISprite>().spriteName=imageName;
			}}}
	public void MagnetLevelsUp(){
		if(Magnet.getInstance().isMagetLock==1){
			for(int j=0;j<(Magnet.getInstance ().magnet_Level);j++){
				GameObject.Find ("MagnetLevel"+(j)).GetComponent<UISprite>().spriteName=imageName;
			}}}

	public static void magicButtonLabel(){
		if(Magic.getInstance().isMagicLock==0){GameObject.Find ("MagicLavelValue").GetComponent<UILabel> ().text = "UnLook";}
		else{GameObject.Find ("MagicLavelValue").GetComponent<UILabel> ().text = (Magic.getInstance ().magic_Level_cost ()).ToString();}
	}
	public static void magnetButtonLabel(){
		if(Magnet.getInstance().isMagetLock==0){GameObject.Find ("MagnetLevelValue").GetComponent<UILabel> ().text = "UnLook";}
		else{GameObject.Find ("MagnetLevelValue").GetComponent<UILabel> ().text = (Magnet.getInstance ().magnet_Level_cost()).ToString();}
	}
	public static void ballScaleUpButtonLabel(){
		if(BallScale.getInstance().isBallScaleLock==0){GameObject.Find ("BallScaleUpLevelValue").GetComponent<UILabel> ().text = "UnLook";}
		else{GameObject.Find ("BallScaleUpLevelValue").GetComponent<UILabel> ().text = (BallScale.getInstance ().ballScale_Level_cost()).ToString();}
	}

//	public static void updateNO_Diamonds(){
//		GameObject.Find ("Diamonds").GetComponent<UILabel> ().text = "Diamonds : "+( PlayerPrefs.GetInt ("diamonds")).ToString();
//	}

	// Update is called once per frame
	void Update () {

		if (IsNotEnoughRes == true) {notEnoughRes.SetActive(true);} 
		else {notEnoughRes.SetActive(false);}
		
//		if(updateDiamonds){
//			GameObject.Find ("Diamonds").GetComponent<UILabel> ().text = "Diamonds : "+( PlayerPrefs.GetInt ("diamonds")).ToString();
//			updateDiamonds=false;
//		}
	
	}
}
