﻿using UnityEngine;
using System.Collections;

public class GameNguiUpdater : MonoBehaviour {
	
	public GameObject GameEnd,magnetTimeShow,ballScaleTimeShow,magicTimeShow,totalDiamonds,distance,Score,Game,SaveMeSeconds,Skip_SaveMe,TimeLeft_Gird,SaveMeLabel;
	public GameObject magnetTimeLeft,magnitTimeLeft,ballScaleUpTimeLeft,notEnoughRes,numOfDiamonds;
	public GameObject Speed;
	public static bool isSphereMove=false;


	public float saveMeSec=3;

	public static bool isGameEnd=false;
	public float GameEnd_seconds = 2.0f;
	public static bool isSaveMe=false,isNotEnoughRes=false;



	void Start () {
		SaveMe.getInstance ().saveMeCount = 0;
		SaveMe.getInstance ().saveMeCount_Costs ();
		isGameEnd=false;
		isSphereMove = false;
		isSaveMe = false;
		isNotEnoughRes=false;
		SaveMeSeconds.SetActive (false);


	}

	public static void playPressed()
	{
		}


	// Update is called once per frame
	void Update () {
	    Speed.GetComponent<UILabel>().text="Speed : "+ System.Math.Round(Player_ZigZag.speed,2);
		if (isNotEnoughRes == true) {notEnoughRes.SetActive(true);} 
		else {notEnoughRes.SetActive(false);}

		
		if (isSaveMe) {
						SaveMeSeconds.SetActive (true);
						saveMeSec = saveMeSec - Time.deltaTime;
						SaveMeSeconds.GetComponent<UILabel> ().text = (((int)saveMeSec)+1).ToString();
			if(saveMeSec<0){

				GameObject.Find("Sphere").GetComponent<Player_ZigZag>().enabled = true;
				Player_ZigZag.speed=1.5f;
				Player_ZigZag.sphearRotation=500;
				if(SaveMeCalls.isXexis){Player_ZigZag.right=false;}else if(SaveMeCalls.isZexis){Player_ZigZag.right=true;}
				//if(GuiCalls.isXexis){Player_ZigZag.right=false;}else if(GuiCalls.isZexis){Player_ZigZag.right=true;}
//				GuiCalls.isXexis=false;
//				GuiCalls.isZexis=false;
				isSaveMe=false;
				saveMeSec=3;

			}
				} else {
						SaveMeSeconds.SetActive (false);
				}

		Score.GetComponent<UILabel>().text ="Distance : "+(PlayerPrefs.GetInt ("distance")).ToString()+"m";

		if (isGameEnd == true) {
			GameEnd_seconds = GameEnd_seconds- Time.deltaTime;
			if (GameEnd_seconds < 1){
				Skip_SaveMe.SetActive(true);
				Game.SetActive(false);
				GoBack.isGame=0;
			    GameEnd.SetActive(true);
				SaveMeLabel.GetComponent<UILabel>().text ="SaveMe "+(SaveMe.getInstance ().saveMeCount_Costs ()).ToString()+"Gems";
				totalDiamonds.GetComponent<UILabel>().text ="Total Diamonds : "+(PlayerPrefs.GetInt ("diamonds")).ToString();
				distance.GetComponent<UILabel>().text ="Distance : "+(PlayerPrefs.GetInt ("distance")).ToString()+"m";
				StoneNguiUpdater.updateDiamonds = true;
				numOfDiamonds.SetActive(true);
		    }

		} 
		else {GameEnd.SetActive(false);}

				if(GetMagnet.magnetOn==true){
			       magnetTimeLeft.SetActive(true);
//			NGUITools.AddChild(TimeLeft_Gird,magnetTimeLeft);
//			UIGrid gridScript = TimeLeft_Gird.GetComponent<UIGrid>();
//			gridScript.Reposition();
			        float barLength = GetMagnet.seconds_Magnet*(100/Magnet.getInstance().magnet_Level_TimeUp());
			        magnetTimeLeft.GetComponent<UISlider>().value = (barLength/100);
			        magnetTimeShow.GetComponent<UILabel> ().text = ((int)GetMagnet.seconds_Magnet).ToString();
		            }else {magnetTimeLeft.SetActive(false);}

				if(GetMagic.MagicOn==true){
			    magnitTimeLeft.SetActive(true);
//			NGUITools.AddChild(TimeLeft_Gird,magnitTimeLeft);
//			UIGrid gridScript = TimeLeft_Gird.GetComponent<UIGrid>();
//			gridScript.Reposition();
			        float barLength = GetMagic.magic_seconds*(100/Magic.getInstance ().magic_Level_TimeUp ());
			        magnitTimeLeft.GetComponent<UISlider>().value = (barLength/100);
			        magicTimeShow.GetComponent<UILabel> ().text = ((int) GetMagic.magic_seconds).ToString();
		            }else {magnitTimeLeft.SetActive(false);}

				if(GetBallScale.ballScaleOn==true){
			      ballScaleUpTimeLeft.SetActive(true);
//			NGUITools.AddChild(TimeLeft_Gird,ballScaleUpTimeLeft);
//			UIGrid gridScript = TimeLeft_Gird.GetComponent<UIGrid>();
//			gridScript.Reposition();
			        float barLength =GetBallScale.ballScale_seconds*(100/BallScale.getInstance().ballScale_Level_TimeUp());
			        ballScaleUpTimeLeft.GetComponent<UISlider>().value = (barLength/100);
			        ballScaleTimeShow.GetComponent<UILabel> ().text =((int)GetBallScale.ballScale_seconds).ToString();
			        }else {ballScaleUpTimeLeft.SetActive(false);}
	
	}
}
