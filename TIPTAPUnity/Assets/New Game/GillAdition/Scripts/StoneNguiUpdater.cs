﻿using UnityEngine;
using System.Collections;

public class StoneNguiUpdater : MonoBehaviour {
	
	public static bool updateDiamonds=false;
	public GameObject TotalDiamonds;
	// Use this for initialization
	void Start () {
		updateDiamonds = true;
	}
	
	// Update is called once per frame
	void Update () {
		if(updateDiamonds){
			TotalDiamonds.transform.GetComponent<UILabel> ().text = "Diamonds : "+( PlayerPrefs.GetInt ("diamonds")).ToString();
			updateDiamonds=false;
		}
	}
}
