﻿using UnityEngine;
using System.Collections;
using System.Linq; 
using System;
using System.IO;

public class Achievements1 : CSVReader {


	public String[,] nameLines;
	public String[,] textLines;

	public int[] RollingValue=new int[18];
	public string[] RollingText=new string[18];
	public int[] Rolling_Achiv_Reward=new int[18];
	public string[] Rolling_Achiv_Ids=new string[18];

	public int rollingAchievementCount=0;
	public int isRollingAch=0;

	public int[] collectGemValue=new int[14];
	public string[] collectGemText=new string[14];
	public int[] collectGem_Achiv_Reward=new int[14];
	public string[] collectGem_Achiv_Ids=new string[14];
	
	public int collectGemAchievementCount=0;
	public int isCollectGemAch=0;

	public int[] PowerUpsValue=new int[15];
	public string[] PowerUpsText=new string[15];
	public int[] whichPowerUp=new int[15];
	public int[] PowerUps_Achiv_Reward=new int[15];
	public string[] PowerUps_Achiv_Ids=new string[15];
	
	public int PowerAchivCount=0;
	public int isPowerUpsAch=0;

	public int amIMagic=0;
	public int amIMagnet=0;
	public int amIBallScale=0;

	public int diamonds;
	public bool isNotColllectDiamnds=true;



	Achievements1()
	{
	}
	
	static Achievements1 myInstance = null;
	
	public static Achievements1 getInstance(){
		if (myInstance == null)
			myInstance = new Achievements1 ();
		return myInstance;
	}



	public void set_Avievements_FomeFile()
	{
		
		TextAsset text=(TextAsset)Resources.Load("Metadata_down the Road", typeof(TextAsset));

		nameLines = base.SplitCsvGrid(text.ToString());
	}


	public void setAchievementsRollingValue(){
		set_Avievements_FomeFile ();
		for (int i=0; i<18; i++) {
			int.TryParse(nameLines[2,i+1], out RollingValue[i]);
			}}

	public void setAchievementsRollig_Text(){ 
		set_Avievements_FomeFile ();
		for (int i=0; i<18; i++) {
			RollingText[i]=nameLines[0,i+1];
		}}
	public void setAchievementsRolling_Reward(){
		set_Avievements_FomeFile ();
		for (int i=0; i<18; i++) {
			int.TryParse(nameLines[1,i+1], out Rolling_Achiv_Reward[i]);
		}}
	public void setAchievementsRolling_IDs(){
		set_Avievements_FomeFile ();
		for (int i=0; i<18; i++) {
			Rolling_Achiv_Ids[i]=nameLines[3,i+1];
		}}



	public void setAchievementsRollings(){
		setAchievementsRollingValue ();
		setAchievementsRollig_Text ();
			if(PlayerPrefs.GetInt("rollingAchievementCount")==3||PlayerPrefs.GetInt("rollingAchievementCount")==6||PlayerPrefs.GetInt("rollingAchievementCount")==9||PlayerPrefs.GetInt("rollingAchievementCount")==12||PlayerPrefs.GetInt("rollingAchievementCount")==15){
				//	if(isNotColllectDiamnds){
				//		if(PlayerPrefs.GetInt("distance")>=RollingValue[PlayerPrefs.GetInt("rollingAchievementCount")]){
					if(PlayerPrefs.GetInt("notDiamondDistance")>=RollingValue[PlayerPrefs.GetInt("rollingAchievementCount")]){
							isRollingAch=1;
							PlayerPrefs.SetInt("isRollingAch",isRollingAch);
						}
				//	}
				}
		}
	public void setAchievementsRolling(){
		setAchievementsRollingValue ();
		setAchievementsRollig_Text ();
//		if(PlayerPrefs.GetInt("rollingAchievementCount")==3||PlayerPrefs.GetInt("rollingAchievementCount")==6||PlayerPrefs.GetInt("rollingAchievementCount")==9||PlayerPrefs.GetInt("rollingAchievementCount")==12||PlayerPrefs.GetInt("rollingAchievementCount")==15){
//		//	if(isNotColllectDiamnds){
//		//		if(PlayerPrefs.GetInt("distance")>=RollingValue[PlayerPrefs.GetInt("rollingAchievementCount")]){
//			if(PlayerPrefs.GetInt("notDiamondDistance")>=RollingValue[PlayerPrefs.GetInt("rollingAchievementCount")]){
//					isRollingAch=1;
//					PlayerPrefs.SetInt("isRollingAch",isRollingAch);
//				}
//		//	}
//		}
		if(PlayerPrefs.GetInt("rollingAchievementCount")!=3&&PlayerPrefs.GetInt("rollingAchievementCount")!=6&&PlayerPrefs.GetInt("rollingAchievementCount")!=9&&PlayerPrefs.GetInt("rollingAchievementCount")!=12&&PlayerPrefs.GetInt("rollingAchievementCount")!=15){
		if(PlayerPrefs.GetInt("distance")>=RollingValue[PlayerPrefs.GetInt("rollingAchievementCount")]){
			isRollingAch=1;
			PlayerPrefs.SetInt("isRollingAch",isRollingAch);
		}
		}
	}
	public void rewardRollngAchiv(){
		diamonds =PlayerPrefs.GetInt ("diamonds");
		setAchievementsRolling_Reward();
		Debug.Log("CONGRATULATION! YOU got"+Rolling_Achiv_Reward[rollingAchievementCount-1]+" Diamonds");
		PlayerPrefs.SetInt ("AchivementReward",Rolling_Achiv_Reward[rollingAchievementCount-1]);
		diamonds=diamonds+PlayerPrefs.GetInt ("AchivementReward");
		PlayerPrefs.SetInt ("diamonds",diamonds);
		StoneNguiUpdater.updateDiamonds = true;
		GameObject.Find ("Achievements").GetComponent<AchievementsNgui>().achivRewardCollect();
		//AchievementsNgui.isAchivRewardCollect=true;
		
		
	}

	public void updateAchivRolling()
	{
		rollingAchievementCount=PlayerPrefs.GetInt("rollingAchievementCount")+1;
		if(rollingAchievementCount>17){rollingAchievementCount=17;}
		PlayerPrefs.SetInt("rollingAchievementCount",rollingAchievementCount);
		GameObject.Find ("Achievements").GetComponent<AchievementsNgui>().updateRollingAchiv();
		//AchievementsNgui.isUpdateRollingAchiv=true;
		GameObject.Find ("Achievements").GetComponent<AchievementsNgui>().rollingAchiv_comp();
	    //AchievementsNgui.isRollingAchiv_comp = true;
	}

	public void setAchievementsCollectGemValue(){
		set_Avievements_FomeFile ();
		for (int i=0; i<13; i++) {
			int.TryParse(nameLines[10,i+1], out collectGemValue[i]);
		}}
	
	public void setAchievements_TextCollectGem(){ 
		set_Avievements_FomeFile ();
		for (int i=0; i<13; i++) {
			collectGemText[i]=nameLines[8,i+1];
		}}
	public void setAchievementsCollectGem_Reward(){
		set_Avievements_FomeFile ();
		for (int i=0; i<13; i++) {
			int.TryParse(nameLines[9,i+1], out collectGem_Achiv_Reward[i]);
		}}
	public void setAchievementsCollectGem_IDs(){
		set_Avievements_FomeFile ();
		for (int i=0; i<13; i++) {
			collectGem_Achiv_Ids[i]=nameLines[11,i+1];
		}}

	public void setAchievementsCollectGem(){
		setAchievementsCollectGemValue ();
		setAchievements_TextCollectGem ();
	        if(PlayerPrefs.GetInt ("CurrentDiamonds")>= collectGemValue[PlayerPrefs.GetInt("collectGemAchievementCount")]){
			isCollectGemAch=1;
			PlayerPrefs.SetInt("isCollectGemAch",isCollectGemAch);
			}
	}
	public void rewardCollectGemAchiv(){
		diamonds =PlayerPrefs.GetInt ("diamonds");
		setAchievementsCollectGem_Reward();
		Debug.Log("CONGRATULATION! YOU got"+collectGem_Achiv_Reward[collectGemAchievementCount-1]+" Diamonds");
		PlayerPrefs.SetInt ("AchivementReward",collectGem_Achiv_Reward[collectGemAchievementCount-1]);
		diamonds=diamonds+PlayerPrefs.GetInt ("AchivementReward");
		PlayerPrefs.SetInt ("diamonds",diamonds);
		StoneNguiUpdater.updateDiamonds = true;
		GameObject.Find ("Achievements").GetComponent<AchievementsNgui>().achivRewardCollect();
	//	AchievementsNgui.isAchivRewardCollect=true;
	}
	public void updateCollectGemAchiv(){
		collectGemAchievementCount=PlayerPrefs.GetInt("collectGemAchievementCount")+1;
	    if(collectGemAchievementCount>=13){collectGemAchievementCount=13;}
		PlayerPrefs.SetInt("collectGemAchievementCount",collectGemAchievementCount);
		GameObject.Find ("Achievements").GetComponent<AchievementsNgui>().updateCollectJemAchiv();
		//AchievementsNgui.isUpdateCollectJemAchiv=true;
		GameObject.Find ("Achievements").GetComponent<AchievementsNgui>().collrctJemsAchiv_Copm();
	//	AchievementsNgui.isCollrctJemsAchiv_Copm = true;
	}

	public void setAchievementsPowerUpsValue(){
		set_Avievements_FomeFile ();
		for (int i=0; i<14; i++) {
			int.TryParse(nameLines[14,i+1], out PowerUpsValue[i]);
		}}
	public void setWhichPowerUp(){
		set_Avievements_FomeFile ();
		for (int i=0; i<14; i++) {
			int.TryParse(nameLines[15,i+1], out whichPowerUp[i]);
		}}
	
	public void setAchievements_TextPowerUps(){ 
		set_Avievements_FomeFile ();
		for (int i=0; i<14; i++) {
			PowerUpsText[i]=nameLines[12,i+1];
		}}
	public void setAchievementsPowerUpsReward(){
		set_Avievements_FomeFile ();
		for (int i=0; i<14; i++) {
			int.TryParse(nameLines[13,i+1], out PowerUps_Achiv_Reward[i]);
		}}
	public void setAchievementsPowerUps_IDs(){
		set_Avievements_FomeFile ();
		for (int i=0; i<14; i++) {
			PowerUps_Achiv_Ids[i]=nameLines[16,i+1];
		}}
	
	public void setAchievementsPowerUps(){
		setAchievementsPowerUpsValue ();
		setAchievements_TextPowerUps ();
		setWhichPowerUp ();
		if (amIMagic == 1) {
		if (whichPowerUp [PlayerPrefs.GetInt ("PowerAchivCount")] == 3) {
				if (PlayerPrefs.GetInt ("isMagicActive")== 1 && Magic.getInstance ().magic_Level >= PowerUpsValue [PlayerPrefs.GetInt ("PowerAchivCount")]) {
					isPowerUpsAch = 1;
					PlayerPrefs.SetInt ("isPowerUpsAch", isPowerUpsAch);
					amIMagic = 0;
				}}}

		if (amIMagnet == 1) {
		if (whichPowerUp [PlayerPrefs.GetInt ("PowerAchivCount")] == 2) {
		if ( PlayerPrefs.GetInt ("isMagnetActive") == 1 && Magnet.getInstance ().magnet_Level >= PowerUpsValue [PlayerPrefs.GetInt ("PowerAchivCount")]) {
					isPowerUpsAch = 1;
					PlayerPrefs.SetInt ("isPowerUpsAch", isPowerUpsAch);
					amIMagnet = 0;
				}}}
		if (amIBallScale == 1) {
		if (whichPowerUp [PlayerPrefs.GetInt ("PowerAchivCount")] == 1) {
				if ( PlayerPrefs.GetInt ("isScaleBallActive") == 1 && BallScale.getInstance ().ballScale_Level >= PowerUpsValue [PlayerPrefs.GetInt ("PowerAchivCount")]) {
			isPowerUpsAch = 1;
			PlayerPrefs.SetInt ("isPowerUpsAch", isPowerUpsAch);
					amIBallScale = 1;
			
		}}}
	}
	public void rewardPowerUpsAchiv(){
		diamonds =PlayerPrefs.GetInt ("diamonds");
		setAchievementsPowerUpsReward();
		Debug.Log("CONGRATULATION! YOU got"+PowerUps_Achiv_Reward[PowerAchivCount-1]+" Diamonds");
		PlayerPrefs.SetInt ("AchivementReward",PowerUps_Achiv_Reward[PowerAchivCount-1]);
		diamonds=diamonds+PlayerPrefs.GetInt ("AchivementReward");
		PlayerPrefs.SetInt ("diamonds",diamonds);
		StoneNguiUpdater.updateDiamonds = true;
		GameObject.Find ("Achievements").GetComponent<AchievementsNgui>().achivRewardCollect();
	//	AchievementsNgui.isAchivRewardCollect=true;
	}
	public void updatePowerAchiv()
	{
		PowerAchivCount=PlayerPrefs.GetInt("PowerAchivCount")+1;
		if(PowerAchivCount>=13){PowerAchivCount=13;}
		PlayerPrefs.SetInt("PowerAchivCount",PowerAchivCount);
		GameObject.Find ("Achievements").GetComponent<AchievementsNgui>().updatePowerAchiv();
	//	AchievementsNgui.isUpdatePowerAchiv=true;
		GameObject.Find ("Achievements").GetComponent<AchievementsNgui>().powerUpsAchiv_comp();
		//AchievementsNgui.isPowerUpsAchiv_comp = true;
	}


}
