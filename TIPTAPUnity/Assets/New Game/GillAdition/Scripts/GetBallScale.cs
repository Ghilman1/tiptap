﻿using UnityEngine;
using System.Collections;

public class GetBallScale : MonoBehaviour {

	public static float ballScale_seconds=5;
	public int counterWatchBigBall=0;



	public Vector3 Original_Ball_scale; 
	public Vector3 Target_Ball_scale;
	public static bool ballScaleOn=false;
	// Use this for initialization
	void Start () {

		counterWatchBigBall = 0;
		ballScaleOn=false;
		ballScale_seconds = BallScale.getInstance().ballScale_Level_TimeUp();

		Original_Ball_scale = new Vector3 (0.3f, 0.3f, 0.3f);
		Target_Ball_scale= new Vector3 (0.6f, 0.6f, 0.6f);

}
	
	// Update is called once per frame
	void Update () {


		if (counterWatchBigBall == 1) {  
			if (transform.localScale != Target_Ball_scale)
				transform.localScale = transform.localScale + new Vector3 (0.01f, 0.01f, 0.01f);
			ballScale_seconds = ballScale_seconds - Time.deltaTime;

			if (ballScale_seconds < 0) {
				ballScale_seconds = 5;
				counterWatchBigBall = 0;
				if(GetMagic.MagicOn==false){
					Player_ZigZag.sphearRotation = PlayerPrefs.GetFloat ("onMagicSphearRotation");
				}else{if(GetMagic.MagicOn){
						Player_ZigZag.sphearRotation=Player_ZigZag.sphearRotation+(Player_ZigZag.sphearRotation*3.5f);
					}}

			}

		} else{ if (transform.localScale != Original_Ball_scale) {
			transform.localScale = transform.localScale - new Vector3 (0.01f, 0.01f, 0.01f);
			ballScaleOn = false;               
		}
	}
				}
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "BallScaler") {
			ballScale_seconds = BallScale.getInstance().ballScale_Level_TimeUp();
			counterWatchBigBall=1;
		    other.gameObject.SetActive (false);
			if(GetMagic.MagicOn==false){
			Player_ZigZag.sphearRotation=PlayerPrefs.GetFloat("onMagicSphearRotation")-(PlayerPrefs.GetFloat("onMagicSphearRotation")/3.5f);
			}else{if(GetMagic.MagicOn){
					Player_ZigZag.sphearRotation=Player_ZigZag.sphearRotation-(Player_ZigZag.sphearRotation/3.5f);
				}}
			ballScaleOn=true;

			}


		
	}


}

