﻿using UnityEngine;
using System.Collections;
using System.Linq; 
using System;
using System.IO;

public class Magic : CSVReader {

	public string imageName="Bright";

	public int[] magic_Costs= new int[6];
    public int  isMagicLock=0;

	public int magic_Level;
	public float[] magic_times = new float[5];


	Magic(){

		 isMagicLock = PlayerPrefs.GetInt ("isMagicActive");
		 magic_Level= PlayerPrefs.GetInt ("magicLevel",0);
		}
    
	static Magic myInstance = null;

	public static Magic getInstance(){
		if (myInstance == null)
						myInstance = new Magic ();
		return myInstance;
	}

	public void magicForStore()
	{
		isMagicLock = PlayerPrefs.GetInt ("isMagicActive");
		magic_Level= PlayerPrefs.GetInt ("magicLevel");
	}

	public void magicLock(){


		if(isMagicLock==0)
		{
			isMagicLock=1;

			PlayerPrefs.SetInt ("isMagicActive", isMagicLock);
			Debug.Log ("Magic is Active");
		
		}
		else {Debug.Log ("Magic is already Active");
		}
		}

	public void set_ScaleCost_FomeFile()
	{
		TextAsset text=(TextAsset)Resources.Load("StoneData", typeof(TextAsset));
		
		String[,] fileLines = base.SplitCsvGrid(text.ToString());
		
		for (int i=0; i<5; i++) 
		{
			int.TryParse(fileLines[i+1,2], out magic_Costs[i]);
			//Debug.Log(magic_Costs[i]);
		}
		
	}
	public void MagicLevelUps()
	{
		if(magic_Level < 5){
			GameObject.Find("MagicLevel"+(magic_Level)).GetComponent<UISprite>().spriteName=imageName;
		}
		magic_Level++;
		if (magic_Level > 5) {   
		//	GameObject.Find ("MagicLabel").GetComponent<UILabel> ().text ="Fully Upgreated!";
			GameObject.Find ("MagicInst").GetComponent<UILabel> ().text ="Fully Upgreated!";
		    magic_Level = 5;
		}

		PlayerPrefs.SetInt ("magicLevel", magic_Level);
//		Debug.Log ("magicLevel No=" + magic_Level);

	}
	
	
	public void set_ScaleTimes_FomeFile()
	{
		TextAsset text=(TextAsset)Resources.Load("StoneData", typeof(TextAsset));
		
		String[,] fileLines = base.SplitCsvGrid(text.ToString());
		
		for (int i=0; i<5; i++) 
		{
			float.TryParse(fileLines[i+1,3], out magic_times[i]);
			//Debug.Log(magic_times[i]);
		}
		
	}
	
// Cost Returns
	public int magic_Level_cost(){
		//int magic_Cost = 0;

			set_ScaleCost_FomeFile ();
			int	magic_Cost = magic_Costs [magic_Level];
			return magic_Cost;
	}
// Time Returns
	public float magic_Level_TimeUp ()
	{
		set_ScaleTimes_FomeFile ();
		float magic_time=0;
		if(isMagicLock==1)
		{
		magic_time = magic_times [magic_Level-1];
	    }
		return magic_time;
	}

	/// <summary>
	/// S	/// </summary>
	/// <returns>The csv grid.</returns>
	/// <param name="csvText">Csv text.</param>
//	static public string[,] SplitCsvGrid(string csvText)
//	{
//		string[] lines = csvText.Split("\n"[0]); 
//		
//		// finds the max width of row
//		int width = 0; 
//		for (int i = 0; i < lines.Length; i++)
//		{
//			string[] row = SplitCsvLine( lines[i] ); 
//			width = Mathf.Max(width, row.Length); 
//		}
//		
//		// creates new 2D string grid to output to
//		string[,] outputGrid = new string[width + 1, lines.Length + 1]; 
//		for (int y = 0; y < lines.Length; y++)
//		{
//			string[] row = SplitCsvLine( lines[y] ); 
//			for (int x = 0; x < row.Length; x++) 
//			{
//				outputGrid[x,y] = row[x]; 
//				
//				// This line was to replace "" with " in my output. 
//				// Include or edit it as you wish.
//				outputGrid[x,y] = outputGrid[x,y].Replace("\"\"", "\"");
//			}
//		}
//		
//		return outputGrid; 
//	}
//	
//	// splits a CSV row 
//	static public string[] SplitCsvLine(string line)
//	{
//		return (from System.Text.RegularExpressions.Match m in System.Text.RegularExpressions.Regex.Matches(line,
//		                                                                                                    @"(((?<x>(?=[,\r\n]+))|""(?<x>([^""]|"""")+)""|(?<x>[^,\r\n]+)),?)", 
//		                                                                                                    System.Text.RegularExpressions.RegexOptions.ExplicitCapture)
//		        select m.Groups[1].Value).ToArray();
//	}
//	//////////


}
