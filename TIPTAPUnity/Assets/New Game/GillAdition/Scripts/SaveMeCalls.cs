﻿using UnityEngine;
using System.Collections;

public class SaveMeCalls : MonoBehaviour {
	public static bool isBuySaveMe=false;
	public GameObject game,numOfDiamonds,sphere,gamePlayDiamonds,camera;
	public static bool isXexis=false, isZexis=false;
	public static Vector3 spherePos;
	public static Vector3 cameraPos;
	public float sphereYPos=0.65f;
//	public static bool isSaveButton=false;
	// Use this for initialization
	void Start () {
		GameObject.Find ("Sphere").GetComponent<Player_ZigZag> ().enabled = false;
		GameObject.Find ("Sphere").GetComponent<Rigidbody>().isKinematic=true;
	//	isSaveButton=false;
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void saveMeCall(){

		Store.getInstance().buySaveMe();
		if (isBuySaveMe){
			
			isBuySaveMe=false;
			//     Debug.Log("current diamonds are "+(PlayerPrefs.GetInt("CurrentDiamonds")));
			GameNguiUpdater.isGameEnd = false;
			game.SetActive (true);
			GoBack.isGameEnd = 0;
			GoBack.isGame = 1;
			numOfDiamonds.SetActive(false);
			if (isXexis == false && isZexis == false) {
				sphere.transform.eulerAngles = new Vector3 (0, 0, 0);
				sphere.transform.position = new Vector3 (-0.064f,0.8f,0.5f);
				sphere.GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.FreezeRotation;
				Player_ZigZag.right=true;
				camera.transform.position = new Vector3 (0, 1.8f,-0.7f);
				GameNguiUpdater.isSaveMe = true;
				isXexis = false ;
				isZexis = false;
				//	diamondLable.SetActive(false);
				gamePlayDiamonds.GetComponent<UILabel> ().text = "Diamonds : "+(PlayerPrefs.GetInt("CurrentDiamonds")).ToString();
				
			} else {
				
				if (isXexis) {
					sphere.transform.eulerAngles = new Vector3 (0, 0, 0);
					sphere.transform.position = new Vector3 (spherePos.x - 0.5f, spherePos.y, cameraPos.z);
				//	sphere.transform.position = new Vector3 (spherePos.x - 0.5f, spherePos.y + 0.7f, cameraPos.z - 0.05f);
					sphere.GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.FreezeRotation;
					camera.transform.position = new Vector3 ((sphere.transform.position.x + 1.5f), cameraPos.y, cameraPos.z);
					GameNguiUpdater.isSaveMe = true;
				} else if (isZexis) {
					sphere.transform.eulerAngles = new Vector3 (0, 0, 0);
					sphere.transform.position = new Vector3 (cameraPos.x, spherePos.y , spherePos.z + 0.5f);
				//	sphere.transform.position = new Vector3 (cameraPos.x + 0.04f, spherePos.y + 0.7f, spherePos.z + 0.5f);
					sphere.GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.FreezeRotation;
					camera.transform.position = new Vector3 (cameraPos.x, cameraPos.y, (sphere.transform.position.z - 1.5f));
					GameNguiUpdater.isSaveMe = true;
				}
			}
			GetMagic.MagicOn=false;
			GetBallScale.ballScaleOn=false;
			GetMagnet.magnetOn=false;
			Achievements1.getInstance ().isNotColllectDiamnds = true;
			FlurryUpdater.saveMeUsed_Count=	FlurryUpdater.saveMeUsed_Count+1;
			
		}

	}
}
