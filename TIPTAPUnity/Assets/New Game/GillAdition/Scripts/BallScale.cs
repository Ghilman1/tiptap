﻿using UnityEngine;
using System.Collections;
using System.Linq; 
using System;
using System.IO;

public class BallScale : CSVReader {
	public string imageName="Bright";

	public int[]   ballScale_Costs= new int[6];
	public int     isBallScaleLock=0;

	public int     ballScale_Level;
	public float[] ballScale_times = new float[5];

	BallScale()
	    {
    	isBallScaleLock = PlayerPrefs.GetInt ("isScaleBallActive");
		ballScale_Level= PlayerPrefs.GetInt ("BallScaleLevel");
		}

	static BallScale myInstance = null;
	public static BallScale getInstance(){
		if (myInstance == null)
			myInstance = new BallScale ();
		return myInstance;
	}
	public void ballScaleForStore()
	{
		isBallScaleLock = PlayerPrefs.GetInt ("isScaleBallActive");
		ballScale_Level= PlayerPrefs.GetInt ("BallScaleLevel");
	}
	//Look 
	public void ballScaleLock(){

		if (isBallScaleLock == 0) {
						isBallScaleLock = 1;
			            PlayerPrefs.SetInt ("isScaleBallActive", isBallScaleLock);
					   Debug.Log ("BallScall is Active");
			
		} else {Debug.Log ("BallScale is already Active");
		}
	    }
	public void ballScaleLevelUps()
	{
		Debug.Log("Greater");
		if(ballScale_Level<5){
		//	GameObject.Find("BallScaleLabel"+(ballScale_Level)).GetComponent<UISprite>().spriteName=imageName;
			GameObject.Find("BallScaleUpLevel"+(ballScale_Level)).GetComponent<UISprite>().spriteName=imageName;

		}
		ballScale_Level++;
		if(ballScale_Level>5){
		//	GameObject.Find ("BallScaleLabel").GetComponent<UILabel> ().text ="Fully Upgreated";
			GameObject.Find ("BallScaleUpInst").GetComponent<UILabel> ().text ="Fully Upgreated";
			ballScale_Level=5;

		} 
	
		PlayerPrefs.SetInt ("BallScaleLevel", ballScale_Level);
//		Debug.Log("BallScaleLevel No="+ballScale_Level);
	
	}
	
	
	public void set_ScaleCost_FomeFile()
	{
		TextAsset text=(TextAsset)Resources.Load("StoneData", typeof(TextAsset));
		
		String[,] fileLines = base.SplitCsvGrid(text.ToString());
		
		for (int i=0; i<5; i++) 
		{
			int.TryParse(fileLines[i+1,0], out ballScale_Costs[i]);
			//Debug.Log(ballScale_Costs[i]);
		}
		
	}


	public void set_ScaleTimes_FomeFile()
	{
		TextAsset text=(TextAsset)Resources.Load("StoneData", typeof(TextAsset));
		
		String[,] fileLines = base.SplitCsvGrid(text.ToString());
				
		for (int i=0; i<5; i++) 
		{
			float.TryParse(fileLines[i+1,1], out ballScale_times[i]);
			//Debug.Log(ballScale_times[i]);
		}
		
	}


	public int ballScale_Level_cost(){
		//int ballScale_Cost=0;

		set_ScaleCost_FomeFile ();
		int	ballScale_Cost = ballScale_Costs [ballScale_Level];
		return ballScale_Cost;
	}

	public float ballScale_Level_TimeUp()
	{
		set_ScaleTimes_FomeFile ();
		float ballScale_time=0;
		if(isBallScaleLock==1)
		{
			ballScale_time = ballScale_times [ballScale_Level-1];
		}
		return ballScale_time;
	}


}
