﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Store {


	public int diamonds;

	Magic       magic;
	BallScale   ballScale;
	Magnet      magnet;
	public bool isChangeToChrome=false,isChangeToCupper=false,isChangeToMarble=false;
	public bool isBuyMarbleMaterial=false,isMagnetBuy=false,isMagicBuy=false,isBallScaleBuy=false,isBuyCupperMaterial=false,isBuyChromeMaterial=false;
	Store()
	{

	//	GuiUpdater.updateDiamonds = true;
	//	UpgradersNgui.updateDiamonds = true;
		StoneNguiUpdater.updateDiamonds = true;

	}
	
	static Store myInstance = null;
	public static Store getInstance(){
		if (myInstance == null)
		myInstance = new Store ();
		return myInstance;
	}



	public void unLockMagnet()
	{
						Magnet.getInstance().magnetLock ();
		                Progression.powerUpsCounter ();

	}
	
	public void unLockBallScale()
	{

						BallScale.getInstance().ballScaleLock ();
		                Progression.powerUpsCounter ();
	}

	public void unLockMagic()
	{

						Magic.getInstance().magicLock ();
		                Progression.powerUpsCounter ();

	}
	public void BuyMagicLevels(){   
		diamonds =PlayerPrefs.GetInt ("diamonds");
		if (diamonds> Magic.getInstance ().magic_Level_cost ()) {
		//	GuiCalls.isMagicBuy=true;
			isMagicBuy=true;
			diamonds = diamonds - (Magic.getInstance ().magic_Level_cost ());
			PlayerPrefs.SetInt ("diamonds",diamonds);
			StoneNguiUpdater.updateDiamonds = true;	
		if (Magic.getInstance ().isMagicLock == 1) {
	        Magic.getInstance ().MagicLevelUps();
			}else { unLockMagic();
	        Magic.getInstance ().MagicLevelUps();
		}
		
		} else {
			UpgradersNgui.IsNotEnoughRes=true;
		}
	}

	public void BuyMagnetLevels()
	{
		diamonds =PlayerPrefs.GetInt ("diamonds");
		if(diamonds>(Magnet.getInstance ().magnet_Level_cost())){
		//	GuiCalls.isMagnetBuy=true;
			isMagnetBuy=false;
			diamonds=diamonds-(Magnet.getInstance ().magnet_Level_cost());
			PlayerPrefs.SetInt ("diamonds",diamonds);
			StoneNguiUpdater.updateDiamonds = true;
		if (Magnet.getInstance ().isMagetLock == 1) {
	        Magnet.getInstance ().MagnetLevelUps();
		    }else {unLockMagnet();
	        Magnet.getInstance ().MagnetLevelUps();
		}

		} else {
			UpgradersNgui.IsNotEnoughRes=true;
			
		}
		
	}
	
	public void BuyBallScaleLevels()
	{
		diamonds =PlayerPrefs.GetInt ("diamonds");
		if(diamonds > (BallScale.getInstance ().ballScale_Level_cost())){
			//GuiCalls.isBallScaleBuy=true;
			isBallScaleBuy=true;
			diamonds=diamonds-(BallScale.getInstance ().ballScale_Level_cost());
			PlayerPrefs.SetInt ("diamonds",diamonds);
		//	GuiUpdater.updateDiamonds = true;
		//	UpgradersNgui.updateDiamonds = true;
			StoneNguiUpdater.updateDiamonds = true;
			
		if (BallScale.getInstance ().isBallScaleLock == 1) {
		    BallScale.getInstance().ballScaleLevelUps();
			}else { unLockBallScale();
		    BallScale.getInstance().ballScaleLevelUps();
		}

		} else {
	//		GuiUpdater.IsNotEnoughRes=true;
			UpgradersNgui.IsNotEnoughRes=true;
			
		}
	}
	public void buySaveMe(){
		diamonds =PlayerPrefs.GetInt ("diamonds");
		if (diamonds > (SaveMe.getInstance ().saveMeCount_Costs ())) {
					//GuiCalls.isBuySaveMe = true;
			           SaveMeCalls.isBuySaveMe=true;
						diamonds = diamonds - (SaveMe.getInstance ().saveMeCount_Costs ());
						PlayerPrefs.SetInt ("diamonds", diamonds);
			            //	UpgradersNgui.updateDiamonds = true;
			            StoneNguiUpdater.updateDiamonds = true;
			            SaveMe.getInstance ().saveMeCount = SaveMe.getInstance ().saveMeCount + 1;
			            if(SaveMe.getInstance ().saveMeCount>4){SaveMe.getInstance ().saveMeCount=4;}
				} else {
						//		GuiUpdater.IsNotEnoughRes=true;
						GameNguiUpdater.isNotEnoughRes = true;
				}
	}
	public void buyChromeMaterial(){
		if (PlayerPrefs.GetInt ("isChromeMaterial") == 0) {
						diamonds = PlayerPrefs.GetInt ("diamonds");
						if (diamonds > (StoneData.getInstance ().metalChrome_Cost ())) {
								//GuiCalls.isBuyChromeMaterial = true;
				                isBuyChromeMaterial = true;
								diamonds = diamonds - (StoneData.getInstance ().metalChrome_Cost ());
								PlayerPrefs.SetInt ("diamonds", diamonds);
				                //	UpgradersNgui.updateDiamonds = true;
				                StoneNguiUpdater.updateDiamonds = true;
				                StoreNguiUpdater._isChangeToChrome=true;
				                StoreNguiUpdater.IsMaterialBuyed=true;
								PlayerPrefs.SetInt ("isChromeMaterial", 1);
				                
						} else {
								GameNguiUpdater.isNotEnoughRes = true;
						}

				} else {
			//GuiCalls.isChangeToChrome=true;
			isChangeToChrome=true;
				}
	}

	public void buyMarbleMaterial(){
		if (PlayerPrefs.GetInt ("isMarbleMaterial") == 0) {
			diamonds = PlayerPrefs.GetInt ("diamonds");
			if (diamonds > (StoneData.getInstance ().marble_cost ())) {
				isBuyMarbleMaterial = true;
				diamonds = diamonds - (StoneData.getInstance ().marble_cost ());
				PlayerPrefs.SetInt ("diamonds", diamonds);
				StoneNguiUpdater.updateDiamonds = true;
				StoreNguiUpdater._isChangeToMarble=true;
				StoreNguiUpdater.IsMaterialBuyed=true;
				PlayerPrefs.SetInt ("isMarbleMaterial", 1);
			} else {
				GameNguiUpdater.isNotEnoughRes = true;
			}
		}else {
			isChangeToMarble=true;
				}
	}


	public void buyCupperMaterial(){
				if (PlayerPrefs.GetInt ("isCupperMaterial") == 0) {
						diamonds = PlayerPrefs.GetInt ("diamonds");
						if (diamonds > (StoneData.getInstance ().metalCopper_Cost ())) {
							//	GuiCalls.isBuyCupperMaterial = true;
				isBuyCupperMaterial = true;
								diamonds = diamonds - (StoneData.getInstance ().metalCopper_Cost ());
								PlayerPrefs.SetInt ("diamonds", diamonds);
				//           UpgradersNgui.updateDiamonds = true;
				                StoneNguiUpdater.updateDiamonds = true;
				                StoreNguiUpdater._isChangeToCupper=true;
				                StoreNguiUpdater.IsMaterialBuyed=true;
				                PlayerPrefs.SetInt ("isCupperMaterial", 1);
			                    } else {

								GameNguiUpdater.isNotEnoughRes = true;
						}

				}
		//GuiCalls.isChangeToCupper = true;
		isChangeToCupper = true;
		}
}

