﻿using UnityEngine;
using System.Collections;

public class GuiCalls : MonoBehaviour {

	Magic       magic;
	BallScale   ballScale;
	Magnet      magnet;
	Store       store;


private int diamonds;
//	public static bool isXexis=false, isZexis=false;
//	public static Vector3 spherePos;
//	public static Vector3 cameraPos;
//	public float sphereYPos=0.65f;
//	public static bool isBuySaveMe = false;
//	public GameObject game,camera;
//	public GameObject diamondLable;
//	public GameObject menu,FbCall,Objectives,_Store,settings,stats,GooglePlayServ,upgraders,home,GameEnd;
//  private string BallScaleButton,SignIn_Out_Button;
//  public static bool isChangeToChrome=false,isChangeToCupper=false,isChangeToMarble=false;
//	public static bool isBuyMarbleMaterial=false;
//	public static bool isBuyCupperMaterial=false,isBallScaleBuy=false,isMagnetBuy=false,isMagicBuy = false;
//  public static bool isBuyChromeMaterial=false;
	public GameObject completeObjectives,currentObjectives,backButton,sphere,AfterNext,skip_saveMe,nextButton,_GoBack;

	public GameObject FbLoginReward,numOfDiamonds,ApplicationQuit;

    
	private string MagicButton,MagnetButton,BallScaleUpButton,CurrentObjectivesButton,CompleteObjectivesButton,ObjectivesButton,OkButton,BackButton,PlayButton,ReplayButton,UpgradersButton,MenuButton,FbRequestButton,GooglePlayServButton,StatsButton,SettingsButton,SaveMeButton,SkipButton,NextButton;
	private string YesQuitButton,NoQuitButton,CollectRollingAchivReward,CollectDiamAchivReward,CollectPorwerAchivReward,StoreButton,AddRemoveButton,LeaderBoardButton,AchievmentsButton,FbButton,FbLogRewButton,CollectRewButton,Mat_ChromeButton,Mat_MarbleButton,Mat_CupperButton,Mat_DefaultButton;
	public string buttonName,Unsend,Reset,Jem1000; 

	public Renderer rand;
	public Material Marble,Cupper,Chrome,M_Default;

	void Start () {

		Jem1000 = "Jem1000";
		Reset = "Reset";
		Unsend = "Unsend";
		YesQuitButton = "YesQuitButton";
		NoQuitButton = "NoQuitButton";
		Mat_DefaultButton = "Mat_DefaultButton";
		Mat_ChromeButton = "Mat_ChromeButton";
		Mat_MarbleButton = "Mat_MarbleButton";
		Mat_CupperButton = "Mat_CupperButton";
		CollectRewButton="CollectRewButton";
		FbLogRewButton = "FbLogRewButton";
		FbButton = "FbButton";
		AchievmentsButton = "AchievmentsButton";
		LeaderBoardButton = "LeaderBoardButton";
		AddRemoveButton = "AddRemoveButton";
		StoreButton = "StoreButton";
		ObjectivesButton = "ObjectivesButton";
		CollectRollingAchivReward = "CollectRollingAchivReward";
		CollectDiamAchivReward = "CollectDiamAchivReward";
		CollectPorwerAchivReward = "CollectPorwerAchivReward";
		CompleteObjectivesButton = "CompleteObjectivesButton";
		CurrentObjectivesButton = "CurrentObjectivesButton";
		NextButton = "NextButton";
		SkipButton = "SkipButton";
		SaveMeButton = "SaveMeButton";
//		SignIn_Out_Button = "SignIn_Out_Button";
//		U_PlayButton = "U_PlayButton";
		SettingsButton = "SettingsButton";
		StatsButton = "StatsButton";
		GooglePlayServButton = "GooglePlayServButton";
		FbRequestButton="FbRequestButton";
		MenuButton = "MenuButton";
		UpgradersButton = "UpgradersButton";
		ReplayButton = "ReplayButton";
//		Replay = "Replay";
		PlayButton = "PlayButton";
		BackButton = "BackButton";
		OkButton = "OkButton";
		MagicButton = "MagicButton";
		MagnetButton = "MagnetButton";
		BallScaleUpButton = "BallScaleUpButton";
//		BallScaleButton = "BallScaleButton";
//		U_BackButton = "U_BackButton";
		buttonName = gameObject.name;

	
	}
	

	void Update () {

	}
	void OnClick(){

		if (buttonName.Equals (ReplayButton)) {
			Application.LoadLevel (1);
			FlurryUpdater.updateFlurry();

		}else if (buttonName.Equals (Jem1000)) {
			diamonds =PlayerPrefs.GetInt ("diamonds");
			diamonds=diamonds+1000;
			PlayerPrefs.SetInt ("diamonds", diamonds);
			StoneNguiUpdater.updateDiamonds = true;
			
		}else if (buttonName.Equals (Reset)) {
			GameObject.Find ("G_P_S").GetComponent<GooglePlayServices>().resetAchievments();
		
		}else if (buttonName.Equals (Unsend)) {
			GameObject.Find ("G_P_S").GetComponent<GooglePlayServices>().UnsendAchievements();

		}else if (buttonName.Equals (YesQuitButton)) {
			Application.Quit (); 
			
		}else if (buttonName.Equals (NoQuitButton)) {
			//Debug.Log (NoQuitButton + " Clicked");
			ApplicationQuit.SetActive(false);
			
		}else if (buttonName.Equals (Mat_DefaultButton)) {
			//Debug.Log (Mat_DefaultButton + " Clicked");
			StoreNguiUpdater.defaultMaterial();
			rand= sphere.GetComponent<Renderer>();
			rand.material=M_Default;
			
		}else if (buttonName.Equals (Mat_MarbleButton)) {
		//	Debug.Log (Mat_MarbleButton + " Clicked");
			Store.getInstance().buyMarbleMaterial();
			if(Store.getInstance().isBuyMarbleMaterial||Store.getInstance().isChangeToMarble){
				StoreNguiUpdater.marbleMaterial();
				rand= sphere.GetComponent<Renderer>();
				rand.material=Marble;
				Store.getInstance().isBuyMarbleMaterial=false;
				Store.getInstance().isChangeToMarble=false;
			}
		}else if (buttonName.Equals (Mat_CupperButton)) {
		//	Debug.Log (Mat_CupperButton + " Clicked");
			Store.getInstance().buyCupperMaterial();
			if(Store.getInstance().isBuyCupperMaterial||Store.getInstance().isChangeToCupper){
				StoreNguiUpdater.cupperMaterial();
				rand= sphere.GetComponent<Renderer>();
				rand.material=Cupper;
				Store.getInstance().isBuyCupperMaterial=false;
				Store.getInstance().isChangeToCupper=false;
			}
		}else if (buttonName.Equals (Mat_ChromeButton)) {
		//	Debug.Log (Mat_ChromeButton + " Clicked");
			Store.getInstance().buyChromeMaterial();
			if(Store.getInstance().isBuyChromeMaterial||Store.getInstance().isChangeToChrome){
				StoreNguiUpdater.chromeMaterial();
				rand= sphere.GetComponent<Renderer>();
				rand.material=Chrome;
				Store.getInstance().isBuyChromeMaterial=false;
				Store.getInstance().isChangeToChrome=false;
			}

		}else if (buttonName.Equals (CollectRewButton)) {
			GameObject.Find ("FbPlugin").GetComponent<FbRollinStone>().fbRewCollButton();
		
		}else if (buttonName.Equals (FbLogRewButton)) {
		//	Debug.Log (FbLogRewButton + " Clicked");
			FbLoginReward.SetActive(false);
		
		}else if (buttonName.Equals (FbButton)){
			GameObject.Find ("FbPlugin").GetComponent<FbRollinStone>().fbLoginButton();
			GoBack.isMenu=0;
			GoBack.isFaceBook=1;
			
		}else if (buttonName.Equals (FbRequestButton)) {
			GameObject.Find ("FbPlugin").GetComponent<FbRollinStone>().fbGameReqButton();
			
		} else if (buttonName.Equals (AchievmentsButton)) {
			GameObject.Find ("G_P_S").GetComponent<GooglePlayServices>().showAchievementsUI();
			
		}else if (buttonName.Equals (LeaderBoardButton)) {
			GameObject.Find ("G_P_S").GetComponent<GooglePlayServices>().showLeaderBoardsUI();
			
		}else if (buttonName.Equals (CollectPorwerAchivReward)) {
			Achievements1.getInstance ().updatePowerAchiv ();
			Achievements1.getInstance().rewardPowerUpsAchiv();
			
		}else if (buttonName.Equals (CollectDiamAchivReward)) {
			Achievements1.getInstance ().updateCollectGemAchiv ();
			Achievements1.getInstance().rewardCollectGemAchiv();
			
		}else if (buttonName.Equals (CollectRollingAchivReward)) {
			Achievements1.getInstance ().updateAchivRolling ();
			Achievements1.getInstance().rewardRollngAchiv();
			
		}else if (buttonName.Equals (CompleteObjectivesButton)) {
			//	Debug.Log (FbLogRewButton + " Clicked");
			currentObjectives.SetActive (false);
			completeObjectives.SetActive (true);

		}else if (buttonName.Equals (CurrentObjectivesButton)) {
			//	Debug.Log (FbLogRewButton + " Clicked");
			currentObjectives.SetActive (true);
			completeObjectives.SetActive (false);
		
		}else if (buttonName.Equals (NextButton)) {
			//	Debug.Log (FbLogRewButton + " Clicked");
			AfterNext.SetActive (true);
			nextButton.SetActive (false);
			backButton.SetActive (false);
			GoBack.isObjectives = 0;
		
		}else if (buttonName.Equals (SkipButton)) {
			//	Debug.Log (FbLogRewButton + " Clicked");
			AdsController.showIntertial();
			GameNguiUpdater.isGameEnd = false;
		    skip_saveMe.SetActive (false);
			nextButton.SetActive (true);
			backButton.SetActive (false);
			GoBack.isObjectives = 1;
		
		}else if (buttonName.Equals (SaveMeButton)) {
			GameObject.Find ("SaveMeCalls").GetComponent<SaveMeCalls>().saveMeCall();

		}else if (buttonName.Equals (ObjectivesButton)) {
			GoBack.isMenu=0;
			GoBack.isObjectives=1;
			
		}else if (buttonName.Equals (AddRemoveButton)) {
		    InApps_Purch.Purchase(InApps_Purch.REMOVE_ADS);
			
		}else if (buttonName.Equals (StoreButton)) {
			//Debug.Log (AddRemoveButton + " Clicked");
			
			GoBack.isMenu=0;
			GoBack.isStore=1;

		}else if (buttonName.Equals (SettingsButton)) {
			//Debug.Log (AddRemoveButton + " Clicked");
			
			GoBack.isMenu=0;
			GoBack.isSettings=1;
		
		}else if (buttonName.Equals (StatsButton)) {
			//Debug.Log (AddRemoveButton + " Clicked");
			
	        GoBack.isMenu=0;
			GoBack.isStats=1;

	    }else if (buttonName.Equals (GooglePlayServButton)) {
			GameObject.Find ("G_P_S").GetComponent<GooglePlayServices>().ConncetButtonPress();
		    GoBack.isMenu=0;
			GoBack.isGooglePlayServ=1;

		}else if (buttonName.Equals (MenuButton)) {
			//Debug.Log (AddRemoveButton + " Clicked");
			
			backButton.SetActive(true);
			GoBack.isHome=0;
			GoBack.isMenu=1;

		}else if (buttonName.Equals (UpgradersButton)) {
			//Debug.Log (AddRemoveButton + " Clicked");
			
			GoBack.isMenu=0;
			GoBack.isUpgraders=1;

		}else if (buttonName.Equals (PlayButton)) {
			//Debug.Log (AddRemoveButton + " Clicked");
			
			numOfDiamonds.SetActive(false);
			PlayerPrefs.SetInt("CurrentDiamonds",0);
			GoBack.sphereMove ();
			GoBack.isGame=1;
			GoBack.isHome=0;
			GoBack.isMenu=0;
			GoBack.isUpgraders=0;

		}else if (buttonName.Equals (BackButton)) {
			GoBack.isBackButton=true;


		}else if (buttonName.Equals (OkButton)) {
			Debug.Log (OkButton + " Clicked");
			UpgradersNgui.IsNotEnoughRes=false;
			GameNguiUpdater.isNotEnoughRes=false;
			StoreNguiUpdater.IsMaterialBuyed=false;
			GameObject.Find ("Achievements").GetComponent<AchievementsNgui>().AchivRewardCollect.SetActive(false);
			//AchievementsNgui.isAchivRewardCollect=false;
			
		}else if (buttonName.Equals (MagicButton)) {
			Debug.Log (MagicButton + " Clicked"); 

			Store.getInstance().BuyMagicLevels();
			if(Store.getInstance().isMagicBuy==true)
			{
				Achievements1.getInstance ().amIMagic=1;
				UpgradersNgui.magicButtonLabel();
				Store.getInstance().isMagicBuy=false;
				Achievements1.getInstance ().setAchievementsPowerUps();
			}

		}else if (buttonName.Equals (MagnetButton)) {
		    Store.getInstance().BuyMagnetLevels();
			Debug.Log ( "Magnet buy1"); 
			if(Store.getInstance().isMagnetBuy==true)
			{
				Achievements1.getInstance().amIMagnet=1;
				UpgradersNgui.magnetButtonLabel();
				Store.getInstance().isMagnetBuy=false;
				Achievements1.getInstance ().setAchievementsPowerUps();
			}
			
		}else if (buttonName.Equals (BallScaleUpButton)) {
			Debug.Log (BallScaleUpButton + " Clicked");

			Store.getInstance().BuyBallScaleLevels();
			if(Store.getInstance().isBallScaleBuy==true)
			{
				Achievements1.getInstance ().amIBallScale=1;
				UpgradersNgui.ballScaleUpButtonLabel();
				Store.getInstance().isBallScaleBuy=false;
				Achievements1.getInstance ().setAchievementsPowerUps();
			}
			
		}
	}
}


