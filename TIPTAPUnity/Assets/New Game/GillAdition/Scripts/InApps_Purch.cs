using UnityEngine;
using System.Collections;

using UnionAssets.FLE;
using System.Collections.Generic;
public class InApps_Purch : MonoBehaviour {

	public static string COINS_ITEM,COINS_BOOST;
	public static bool _isInited;
	public const string REMOVE_ADS =  "remove_ads_id";

	public const string base64EncodedPublicKey ="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvQylFQEoQnDwA8tAstlcpja4U8w4Ue+6nakK8mn5+OgydkCUDB1FmXPTJUmSl6CLT7svbvbUowJel0+nREP1baqqxXZK9lPBbsKDcfHyZMmwzW9GxQzkECQSyV0GU0T6uHUF8cWA95c/OSxjdCa4RJAepZI/S7v0wvkss7gRiGCaVwZBYNH6v7AS+4hgTagem/Ln3PEV63KN8RxZcSvTBxOk0DR/r3If5fcJ7O4Fh3AVD5sR+EdW7KreeOAigtxgustAXmMZxdH9oYNo/UkqfUx5SHOXo97KaD9K1g3w/CQZ64cwSIXKZEo32yXAu79v13LJ8P75yfaTt0DiL88/aQIDAQAB";
	
	private static bool ListnersAdded = false;

	void Start () {
	


		if(ListnersAdded) {
			return;
		}
		AndroidInAppPurchaseManager.instance.addProduct (REMOVE_ADS);

		//listening for purchase and consume events
		AndroidInAppPurchaseManager.ActionProductPurchased += OnProductPurchased;  
		AndroidInAppPurchaseManager.ActionProductConsumed  += OnProductConsumed;
		
		
		//listening for store initilaizing finish
		AndroidInAppPurchaseManager.ActionBillingSetupFinished += OnBillingConnected;

		//AndroidInAppPurchaseManager.instance.loadStore();
		
		AndroidInAppPurchaseManager.instance.loadStore(base64EncodedPublicKey);

	

		ListnersAdded = true;
	}
//	    public static void load_Store(){
//		}
	public static void Purchase(string SKU) {
		AndroidInAppPurchaseManager.instance.purchase (SKU);
	}

	private static void OnBillingConnected(BillingResult result) {
		AndroidInAppPurchaseManager.ActionBillingSetupFinished -= OnBillingConnected;
		
		if(result.isSuccess) {
			//Store connection is Successful. Next we loading product and customer purchasing details
			AndroidInAppPurchaseManager.instance.retrieveProducDetails();
			AndroidInAppPurchaseManager.ActionRetrieveProducsFinished += OnRetriveProductsFinised;
		} 
		
	//	AndroidMessage.Create("Connection Responce", result.response.ToString() + " " + result.message);
	//	Debug.Log ("Connection Responce: " + result.response.ToString() + " " + result.message);
	}

	private static void OnRetriveProductsFinised(BillingResult result) {
		AndroidInAppPurchaseManager.ActionRetrieveProducsFinished -= OnRetriveProductsFinised;
		
		if(result.isSuccess) {
			_isInited = true;
	//		AndroidMessage.Create("Success", "Billing init complete inventory contains: " + AndroidInAppPurchaseManager.instance.inventory.purchases.Count + " products");
		} else {
	//		AndroidMessage.Create("Connection Responce", result.response.ToString() + " " + result.message);
		}
	//	Debug.Log ("Connection Responce: " + result.response.ToString() + " " + result.message);
		
	}
	
	private static void OnProductPurchased(BillingResult result) {
		if(result.isSuccess) {
		//	OnProcessingPurchasedProduct (result.purchase);
			if(result.purchase.SKU == REMOVE_ADS){
			  //GameStore.GetInstance().BuyAds();
			}
		} else {
	//		AndroidMessage.Create("Product Purchase Failed", result.response.ToString() + " " + result.message);
		}
	//	Debug.Log ("Purchased Responce: " + result.response.ToString() + " " + result.message);
	}


	private static void OnProductConsumed(BillingResult result){

		}
	private static void OnProcessingPurchasedProduct(GooglePurchaseTemplate purchase) {

		if(purchase.SKU == REMOVE_ADS){
			//	GameStore.GetInstance().BuyAds();
		}
//		switch(purchase.SKU) {
//		case COINS_ITEM:
//			AndroidInAppPurchaseManager.instance.consume(CONIS_PACK);
//			break;
//		case BONUS_TRACK:
//			GameData.UnlockBonusTrack();
//			break;
//		}
	}
	
	
//	private static void OnProcessingConsumeProduct(GooglePurchaseTemplate purchase) {
////		switch(purchase.SKU) {
////		case CONIS_PACK:
////			GameData.AddCoins(100);
////			break;
////		}
//	}
	// Update is called once per frame
	void Update () {
	
	}
}
