﻿using UnityEngine;
using System.Collections;

public class NextScene : MonoBehaviour {

	public Object[] Scene;
	public float Sec=3;
	public string SceneName;
	public GameObject LoadindSreen,LoadingBar;
	public bool isLoadScreen = false;
	private AsyncOperation loadOp;
	//private AsyncOperation loadOp = null;
	// Use this for initialization
	void Start () {
	//	DontDestroyOnLoad(transform.gameObject);
		SceneName = Scene[0].name;
		LoadindSreen.SetActive (false);

	}
	
	// Update is called once per frame
	void Update () {

		if(Sec<=3){
		Sec = Sec - Time.deltaTime;
		}
		if (Sec < 0) {
			LoadindSreen.SetActive(true);
			isLoadScreen = true;
			Sec=4;
				}
		if(isLoadScreen){
		//  loadOp=Application.LoadLevelAsync(1); 
	  
		// LoadingBar.GetComponent<UISlider> ().value=Application.GetStreamProgressForLevel(SceneName);
		//	Debug.Log("Loading Value is:" +Application.CanStreamedLevelBeLoaded(1));
		
	
			StartCoroutine(LoadALevel(1));
//				Application.LoadLevel(1);
			 //  LoadALevel(1);
			 //   LoadingBar.GetComponent<UISlider> ().value=loadOp.progress*100;

			isLoadScreen=false;
		}
	}
	private IEnumerator LoadALevel(int number) {
		Debug.Log("Scene Name is:" + SceneName);
		loadOp = Application.LoadLevelAsync(number);
		while(loadOp.progress!=1) {
			yield return loadOp.progress;
			LoadingBar.GetComponent<UISlider> ().value=loadOp.progress;
				}
	}
}
