﻿using UnityEngine;
using System.Collections;

public class AchievementsNgui : MonoBehaviour {

	//public static bool isUpdateRollingAchiv;
	//public static bool isUpdateCollectJemAchiv;
	//public static bool isUpdatePowerAchiv;
	//public static bool isAchivRewardCollect=false;
	//public static bool isCollrctJemsAchiv_Copm=false,isRollingAchiv_comp=false;
	//public static bool isPowerUpsAchiv_comp=false;
	
	public static bool isShowComp_Achiv;

	public GameObject CompleteAchiGrid;
	//public UIGrid CompleteAchiGrid;
	public int CompletedAchCount=0;
	public GameObject CompletedAchievementCln,CompletedAchievement,ComleteAchGrid,RollAcivCheck,PowerAcivCheck,CollectJemCheck,NextRollAchiv,NextPowerAchiv,NextClloectJemAchiv;

	public GameObject rollingAchvText,collectGemAchText,powerUpsAchText,AchivRewardCollect;
    public GameObject rollingAchvReward,collectGemAchvReward,powerUpsAchReward;


	public int comp_Achiv_Count;



	void Start () {

	//	isAchivRewardCollect=false;
		comp_Achiv_Count = PlayerPrefs.GetInt ("rollingAchievementCount") + PlayerPrefs.GetInt("collectGemAchievementCount") + PlayerPrefs.GetInt("PowerAchivCount");
		isShowComp_Achiv = false;

	//	isUpdateRollingAchiv = false;
		CompletedAchCount=PlayerPrefs.GetInt("CompletedAchCount");
      //  PlayerPrefs.SetInt ("rollingAchievementCount",0);

		Achievements1.getInstance ().setAchievementsRollig_Text ();
		Achievements1.getInstance ().setAchievements_TextCollectGem ();
		Achievements1.getInstance ().setAchievements_TextPowerUps ();
		Achievements1.getInstance ().setAchievementsRolling_IDs ();
		Achievements1.getInstance ().setAchievementsCollectGem_IDs ();
		Achievements1.getInstance ().setAchievementsPowerUps_IDs ();
		Achievements1.getInstance ().setAchievementsRolling_Reward();
		Achievements1.getInstance ().setAchievementsCollectGem_Reward ();
		Achievements1.getInstance ().setAchievementsPowerUpsReward ();


		rollingAchvText.GetComponent<UILabel> ().text =  Achievements1.getInstance ().RollingText [PlayerPrefs.GetInt("rollingAchievementCount")];
		collectGemAchText.GetComponent<UILabel> ().text =  Achievements1.getInstance ().collectGemText[PlayerPrefs.GetInt("collectGemAchievementCount")];
		powerUpsAchText.GetComponent<UILabel> ().text =  Achievements1.getInstance ().PowerUpsText[PlayerPrefs.GetInt("PowerAchivCount")];
		rollingAchvReward.GetComponent<UILabel> ().text =  "For "+(Achievements1.getInstance ().Rolling_Achiv_Reward[PlayerPrefs.GetInt("rollingAchievementCount")]).ToString()+" Gems";
		collectGemAchvReward.GetComponent<UILabel> ().text ="For "+(Achievements1.getInstance ().collectGem_Achiv_Reward [PlayerPrefs.GetInt("rollingAchievementCount")]).ToString()+" Gems";
		powerUpsAchReward.GetComponent<UILabel> ().text = "For "+ (Achievements1.getInstance ().PowerUps_Achiv_Reward [PlayerPrefs.GetInt("rollingAchievementCount")]).ToString()+" Gems";
//		Debug.Log (Achievements1.getInstance ().RollingText [17]);
//		Debug.Log (PlayerPrefs.GetInt("rollingAchievementCount"));

		for (int i=0; i<PlayerPrefs.GetInt ("rollingAchievementCount"); i++) {
			CompletedAchievement.transform.GetChild(1).GetComponent<UILabel>().text=  Achievements1.getInstance ().RollingText [i];
			NGUITools.AddChild(CompleteAchiGrid,CompletedAchievement);
		    UIGrid gridScript = CompleteAchiGrid.GetComponent<UIGrid>();
			gridScript.Reposition();

		}
		for (int i=0; i<PlayerPrefs.GetInt("collectGemAchievementCount"); i++) {
			CompletedAchievement.transform.GetChild(1).GetComponent<UILabel>().text= Achievements1.getInstance ().collectGemText [i];
			NGUITools.AddChild(CompleteAchiGrid,CompletedAchievement);
		    UIGrid gridScript = CompleteAchiGrid.GetComponent<UIGrid>();
			gridScript.Reposition();

		}
		for (int i=0; i<PlayerPrefs.GetInt("PowerAchivCount"); i++) {
			CompletedAchievement.transform.GetChild(1).GetComponent<UILabel>().text= Achievements1.getInstance ().PowerUpsText [i];
			NGUITools.AddChild(CompleteAchiGrid,CompletedAchievement);
			UIGrid gridScript = CompleteAchiGrid.GetComponent<UIGrid>();
			gridScript.Reposition();

		}
	}
	public void achivRewardCollect(){
		AchivRewardCollect.SetActive(true);
		AchivRewardCollect.transform.GetChild(1).GetComponent<UILabel>().text= "you have got "+PlayerPrefs.GetInt ("AchivementReward")+" diamonds...";
		}

	public void updateRollingAchiv(){
		//Achievements1.getInstance ().setAchievementsRollings ();
		rollingAchvText.GetComponent<UILabel> ().text = Achievements1.getInstance ().RollingText [PlayerPrefs.GetInt ("rollingAchievementCount")];
		rollingAchvReward.GetComponent<UILabel> ().text =  "For "+(Achievements1.getInstance ().Rolling_Achiv_Reward[PlayerPrefs.GetInt("rollingAchievementCount")]).ToString()+" Gems";
		//	Debug.Log(Achievements1.getInstance ().Rolling_Achiv_Ids [PlayerPrefs.GetInt ("rollingAchievementCount")-1]);
		PlayerPrefs.SetString("AchievementID",Achievements1.getInstance ().Rolling_Achiv_Ids [PlayerPrefs.GetInt ("rollingAchievementCount")-1]);
		GameObject.Find ("G_P_S").GetComponent<GooglePlayServices>().unlookAchievement();
		//GooglePlayServices.isUnlookAchievement=true;
		//GooglePlayServices.unlookAchievement(Achievements1.getInstance ().Rolling_Achiv_Ids[PlayerPrefs.GetInt("rollingAchievementCount")-1]);
		Achievements1.getInstance ().isRollingAch = 0;
		PlayerPrefs.SetInt("isRollingAch",0);
		}
	public void updateCollectJemAchiv(){
		collectGemAchText.GetComponent<UILabel> ().text =  Achievements1.getInstance ().collectGemText[PlayerPrefs.GetInt("collectGemAchievementCount")];
		collectGemAchvReward.GetComponent<UILabel> ().text ="For "+(Achievements1.getInstance ().collectGem_Achiv_Reward [PlayerPrefs.GetInt("rollingAchievementCount")]).ToString()+" Gems";
		PlayerPrefs.SetString("AchievementID",Achievements1.getInstance ().collectGem_Achiv_Ids[PlayerPrefs.GetInt("collectGemAchievementCount")-1]);
		GameObject.Find ("G_P_S").GetComponent<GooglePlayServices>().unlookAchievement();
		//GooglePlayServices.isUnlookAchievement=true;
		Achievements1.getInstance ().isCollectGemAch=0;
		PlayerPrefs.SetInt("isCollectGemAch",0);
      }
	public void updatePowerAchiv(){
		powerUpsAchText.GetComponent<UILabel> ().text =  Achievements1.getInstance ().PowerUpsText [PlayerPrefs.GetInt("PowerAchivCount")];
		powerUpsAchReward.GetComponent<UILabel> ().text = "For "+ (Achievements1.getInstance ().PowerUps_Achiv_Reward [PlayerPrefs.GetInt("rollingAchievementCount")]).ToString()+" Gems";
		PlayerPrefs.SetString("AchievementID",Achievements1.getInstance ().PowerUps_Achiv_Ids[PlayerPrefs.GetInt("PowerAchivCount")-1]);
		GameObject.Find ("G_P_S").GetComponent<GooglePlayServices>().unlookAchievement();
		//GooglePlayServices.isUnlookAchievement=true;
		Achievements1.getInstance ().isPowerUpsAch=0;
		PlayerPrefs.SetInt("isPowerUpsAch",0);
		//isUpdatePowerAchiv=false;
		if(Magic.getInstance ().magic_Level >= Achievements1.getInstance().PowerUpsValue [PlayerPrefs.GetInt ("PowerAchivCount")]&&Achievements1.getInstance().whichPowerUp [PlayerPrefs.GetInt ("PowerAchivCount")] == 3){
			Achievements1.getInstance().isPowerUpsAch = 1;
			PlayerPrefs.SetInt ("isPowerUpsAch",  Achievements1.getInstance().isPowerUpsAch);
		}
		if(Magnet.getInstance ().magnet_Level >= Achievements1.getInstance().PowerUpsValue [PlayerPrefs.GetInt ("PowerAchivCount")]&&Achievements1.getInstance().whichPowerUp [PlayerPrefs.GetInt ("PowerAchivCount")] == 2){
			Achievements1.getInstance().isPowerUpsAch = 1;
			PlayerPrefs.SetInt ("isPowerUpsAch",  Achievements1.getInstance().isPowerUpsAch);
		}
		if(BallScale.getInstance ().ballScale_Level >= Achievements1.getInstance().PowerUpsValue [PlayerPrefs.GetInt ("PowerAchivCount")]&&Achievements1.getInstance().whichPowerUp [PlayerPrefs.GetInt ("PowerAchivCount")] == 1){
			Achievements1.getInstance().isPowerUpsAch = 1;
			PlayerPrefs.SetInt ("isPowerUpsAch",  Achievements1.getInstance().isPowerUpsAch);
		}
		}
	public void rollingAchiv_comp(){
		CompletedAchievement.transform.GetChild(1).GetComponent<UILabel>().text=  Achievements1.getInstance ().RollingText[PlayerPrefs.GetInt ("rollingAchievementCount")-1];
		NGUITools.AddChild(CompleteAchiGrid,CompletedAchievement);
		UIGrid gridScript = CompleteAchiGrid.GetComponent<UIGrid>();
		gridScript.Reposition();
		}
	public void collrctJemsAchiv_Copm(){
		CompletedAchievement.transform.GetChild(1).GetComponent<UILabel>().text=  Achievements1.getInstance ().collectGemText[PlayerPrefs.GetInt("collectGemAchievementCount")-1];
		NGUITools.AddChild(CompleteAchiGrid,CompletedAchievement);
		UIGrid gridScript = CompleteAchiGrid.GetComponent<UIGrid>();
		gridScript.Reposition();
		}
	public void powerUpsAchiv_comp(){
		CompletedAchievement.transform.GetChild(1).GetComponent<UILabel>().text= Achievements1.getInstance ().PowerUpsText[PlayerPrefs.GetInt("PowerAchivCount")-1];
		NGUITools.AddChild(CompleteAchiGrid,CompletedAchievement);
		UIGrid gridScript = CompleteAchiGrid.GetComponent<UIGrid>();
		gridScript.Reposition();
	}
	// Update is called once per frame
	void Update () {

//		if (isAchivRewardCollect == true) {
//			AchivRewardCollect.SetActive(true);
//			AchivRewardCollect.transform.GetChild(1).GetComponent<UILabel>().text= "you have got "+PlayerPrefs.GetInt ("AchivementReward")+" diamonds...";
//		} 
//		else {AchivRewardCollect.SetActive(false);}
		
//		if(isAchivRewardCollect){
//
//			isAchivRewardCollect=false;
//		}

		Achievements1.getInstance ().setAchievementsRollings ();

		if (PlayerPrefs.GetInt ("isRollingAch") == 1) {
			RollAcivCheck.SetActive (true);
			NextRollAchiv.SetActive (true);
		} else {
			RollAcivCheck.SetActive (false);
			NextRollAchiv.SetActive (false);
		}
		if (PlayerPrefs.GetInt ("isCollectGemAch") == 1) {
			CollectJemCheck.SetActive (true);
			NextClloectJemAchiv.SetActive (true);
		} else {
			CollectJemCheck.SetActive (false);
			NextClloectJemAchiv.SetActive (false);
		}
		if (PlayerPrefs.GetInt ("isPowerUpsAch") == 1) {
			PowerAcivCheck.SetActive (true);
			NextPowerAchiv.SetActive (true);
		} else {
			PowerAcivCheck.SetActive (false);
			NextPowerAchiv.SetActive (false);
		}
//
//		if (isUpdateRollingAchiv) {
//		    rollingAchvText.GetComponent<UILabel> ().text = Achievements1.getInstance ().RollingText [PlayerPrefs.GetInt ("rollingAchievementCount")];
//			rollingAchvReward.GetComponent<UILabel> ().text =  "For "+(Achievements1.getInstance ().Rolling_Achiv_Reward[PlayerPrefs.GetInt("rollingAchievementCount")]).ToString()+" Gems";
//			
//		//	Debug.Log(Achievements1.getInstance ().Rolling_Achiv_Ids [PlayerPrefs.GetInt ("rollingAchievementCount")-1]);
//			PlayerPrefs.SetString("AchievementID",Achievements1.getInstance ().Rolling_Achiv_Ids [PlayerPrefs.GetInt ("rollingAchievementCount")-1]);
//			GooglePlayServices.isUnlookAchievement=true;
//			//GooglePlayServices.unlookAchievement(Achievements1.getInstance ().Rolling_Achiv_Ids[PlayerPrefs.GetInt("rollingAchievementCount")-1]);
//			Achievements1.getInstance ().isRollingAch = 0;
//			PlayerPrefs.SetInt("isRollingAch",0);
//			isUpdateRollingAchiv=false;
//            }



//		if (isUpdateCollectJemAchiv) {
//			collectGemAchText.GetComponent<UILabel> ().text =  Achievements1.getInstance ().collectGemText[PlayerPrefs.GetInt("collectGemAchievementCount")];
//		    collectGemAchvReward.GetComponent<UILabel> ().text ="For "+(Achievements1.getInstance ().collectGem_Achiv_Reward [PlayerPrefs.GetInt("rollingAchievementCount")]).ToString()+" Gems";
//			PlayerPrefs.SetString("AchievementID",Achievements1.getInstance ().collectGem_Achiv_Ids[PlayerPrefs.GetInt("collectGemAchievementCount")-1]);
//			GooglePlayServices.isUnlookAchievement=true;
//			Achievements1.getInstance ().isCollectGemAch=0;
//			PlayerPrefs.SetInt("isCollectGemAch",0);
//			isUpdateCollectJemAchiv=false;
//		}



//		if (isUpdatePowerAchiv) {
//			powerUpsAchText.GetComponent<UILabel> ().text =  Achievements1.getInstance ().PowerUpsText [PlayerPrefs.GetInt("PowerAchivCount")];
//		    powerUpsAchReward.GetComponent<UILabel> ().text = "For "+ (Achievements1.getInstance ().PowerUps_Achiv_Reward [PlayerPrefs.GetInt("rollingAchievementCount")]).ToString()+" Gems";
//			PlayerPrefs.SetString("AchievementID",Achievements1.getInstance ().PowerUps_Achiv_Ids[PlayerPrefs.GetInt("PowerAchivCount")-1]);
//			GooglePlayServices.isUnlookAchievement=true;
//			Achievements1.getInstance ().isPowerUpsAch=0;
//			PlayerPrefs.SetInt("isPowerUpsAch",0);
//			isUpdatePowerAchiv=false;
//			if(Magic.getInstance ().magic_Level >= Achievements1.getInstance().PowerUpsValue [PlayerPrefs.GetInt ("PowerAchivCount")]&&Achievements1.getInstance().whichPowerUp [PlayerPrefs.GetInt ("PowerAchivCount")] == 3){
//				Achievements1.getInstance().isPowerUpsAch = 1;
//				PlayerPrefs.SetInt ("isPowerUpsAch",  Achievements1.getInstance().isPowerUpsAch);
//			}
//			if(Magnet.getInstance ().magnet_Level >= Achievements1.getInstance().PowerUpsValue [PlayerPrefs.GetInt ("PowerAchivCount")]&&Achievements1.getInstance().whichPowerUp [PlayerPrefs.GetInt ("PowerAchivCount")] == 2){
//				Achievements1.getInstance().isPowerUpsAch = 1;
//				PlayerPrefs.SetInt ("isPowerUpsAch",  Achievements1.getInstance().isPowerUpsAch);
//			}
//			if(BallScale.getInstance ().ballScale_Level >= Achievements1.getInstance().PowerUpsValue [PlayerPrefs.GetInt ("PowerAchivCount")]&&Achievements1.getInstance().whichPowerUp [PlayerPrefs.GetInt ("PowerAchivCount")] == 1){
//				Achievements1.getInstance().isPowerUpsAch = 1;
//				PlayerPrefs.SetInt ("isPowerUpsAch",  Achievements1.getInstance().isPowerUpsAch);
//			}
//		}
//		if(isRollingAchiv_comp){
//			CompletedAchievement.transform.GetChild(1).GetComponent<UILabel>().text=  Achievements1.getInstance ().RollingText[PlayerPrefs.GetInt ("rollingAchievementCount")-1];
//			NGUITools.AddChild(CompleteAchiGrid,CompletedAchievement);
//		    UIGrid gridScript = CompleteAchiGrid.GetComponent<UIGrid>();
//			gridScript.Reposition();
//			isRollingAchiv_comp=false;
//		}
//		if(isCollrctJemsAchiv_Copm){
//			CompletedAchievement.transform.GetChild(1).GetComponent<UILabel>().text=  Achievements1.getInstance ().collectGemText[PlayerPrefs.GetInt("collectGemAchievementCount")-1];
//			NGUITools.AddChild(CompleteAchiGrid,CompletedAchievement);
//			UIGrid gridScript = CompleteAchiGrid.GetComponent<UIGrid>();
//			gridScript.Reposition();
//			isCollrctJemsAchiv_Copm=false;
//		}
//		if(isPowerUpsAchiv_comp){
//			CompletedAchievement.transform.GetChild(1).GetComponent<UILabel>().text= Achievements1.getInstance ().PowerUpsText[PlayerPrefs.GetInt("PowerAchivCount")-1];
//			NGUITools.AddChild(CompleteAchiGrid,CompletedAchievement);
//			UIGrid gridScript = CompleteAchiGrid.GetComponent<UIGrid>();
//			gridScript.Reposition();
//		    isPowerUpsAchiv_comp=false;
//		}


	}
}
