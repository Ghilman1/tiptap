﻿using UnityEngine;
using System.Collections;

public class GetMagic : MonoBehaviour {

	public bool counterWatchMagic = false;
	public static float magic_seconds=0;
	public static bool MagicOn=false;
	// Use this for initialization
	void Start () {
		MagicOn=false;
		magic_seconds = Magic.getInstance ().magic_Level_TimeUp ();
		Player_ZigZag.inputEnable = true;
	}

	// Update is called once per frame
	void Update () {

		if(counterWatchMagic)
		{
			magic_seconds = magic_seconds - Time.deltaTime;
			
			if (magic_seconds < 0) {
				magic_seconds =0 ;
				counterWatchMagic=false;
				Player_ZigZag.inputEnable = true;
				if(GetBallScale.ballScaleOn==false){
					Player_ZigZag.speed=PlayerPrefs.GetFloat ("onMagicSpeed");
					Player_ZigZag.sphearRotation=PlayerPrefs.GetFloat ("onMagicSphearRotation");
				}else{if(GetBallScale.ballScaleOn){
						Player_ZigZag.speed=Player_ZigZag.speed/2.0f;
						Player_ZigZag.sphearRotation=Player_ZigZag.sphearRotation/2.0f;
					}}

				MagicOn=false;
			}
		}
		if(MagicOn==false && GetMagnet.magnetOn==false && GetBallScale.ballScaleOn==false)
		{
			ZigZagTileSpawner.Prob=0.2f;
		}
	
	}
	void OnTriggerEnter(Collider other1)
	{
				if (other1.gameObject.tag == "Magic") {
//			       Debug.Log ("Magic Enable");
			magic_seconds = Magic.getInstance ().magic_Level_TimeUp ();
			           other1.gameObject.SetActive (false);
			           counterWatchMagic=true;
			           Player_ZigZag.inputEnable = false;
//			Debug.Log(PlayerPrefs.GetFloat ("onMagicSpeed"));
//			Debug.Log(PlayerPrefs.GetFloat ("onMagicSphearRotation"));
			MagicOn=true;
			Player_ZigZag.speed=PlayerPrefs.GetFloat ("onMagicSpeed")*2.0f;
			if(Player_ZigZag.speed>11){Player_ZigZag.speed=11;}
			if(GetBallScale.ballScaleOn==false){
			Player_ZigZag.sphearRotation=PlayerPrefs.GetFloat("onMagicSphearRotation")*2.0f;
			}else{if(GetBallScale.ballScaleOn){
					Player_ZigZag.sphearRotation=Player_ZigZag.sphearRotation*2.0f;
				}
			}
			         }
		

		}


}
