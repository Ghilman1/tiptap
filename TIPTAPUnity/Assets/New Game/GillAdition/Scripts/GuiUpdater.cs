﻿using UnityEngine;
using System.Collections;

public class GuiUpdater : MonoBehaviour {


	//public GameObject GameEnd;
	public string imageName="Bright";
	public   GameObject notEnoughRes;
	public static bool IsNotEnoughRes=false;
	public static bool updateDiamonds=false;

	void Start () {
	   GuiUpdater.updateDiamonds = true;


		ballScalelevelsUp();
		magicLevelsUp ();
		MagnetLevelsUp ();

		magicButtonLabel ();
		magnetButtonLabel();
		ballScaleButtonLabel ();

	
	}

	public void ballScalelevelsUp(){
		if( BallScale.getInstance().isBallScaleLock==1){
			for(int k=0;k<(BallScale.getInstance ().ballScale_Level);k++){
				GameObject.Find ("BallScaleLevel"+(k)).GetComponent<UISprite>().spriteName=imageName;
			}}}


	public void magicLevelsUp(){
		if(Magic.getInstance().isMagicLock==1){
			for(int i=0;i<(Magic.getInstance ().magic_Level);i++){
				GameObject.Find ("MagicLevel"+(i)).GetComponent<UISprite>().spriteName=imageName;
			}}}

	public void MagnetLevelsUp(){
		if(Magnet.getInstance().isMagetLock==1){
			for(int j=0;j<(Magnet.getInstance ().magnet_Level);j++){
				GameObject.Find ("MagnetLevel"+(j)).GetComponent<UISprite>().spriteName=imageName;
			}}}

	public static void magicButtonLabel(){
		if(Magic.getInstance().isMagicLock==0){GameObject.Find ("MagicLavelValue").GetComponent<UILabel> ().text = "UnLook";}
		else{GameObject.Find ("MagicLavelValue").GetComponent<UILabel> ().text = (Magic.getInstance ().magic_Level_cost ()).ToString();}
	}

	public static void magnetButtonLabel(){
		if(Magnet.getInstance().isMagetLock==0){GameObject.Find ("MagnetLevelValue").GetComponent<UILabel> ().text = "UnLook";}
		else{GameObject.Find ("MagnetLevelValue").GetComponent<UILabel> ().text = (Magnet.getInstance ().magnet_Level_cost()).ToString();}
	}

	public static void ballScaleButtonLabel(){
		if(BallScale.getInstance().isBallScaleLock==0){GameObject.Find ("BallScaleLevelValue").GetComponent<UILabel> ().text = "UnLook";}
		else{GameObject.Find ("BallScaleLevelValue").GetComponent<UILabel> ().text = (BallScale.getInstance ().ballScale_Level_cost()).ToString();}
	}


	public static void updateNO_Diamonds(){
		GameObject.Find ("Diamonds").GetComponent<UILabel> ().text = "Diamonds : "+( PlayerPrefs.GetInt ("diamonds")).ToString();
		}

	public static void notEnoughResr(){
		
	}
	  void Update () {
		if (IsNotEnoughRes == true) {notEnoughRes.SetActive(true);} 
		else {notEnoughRes.SetActive(false);}

		if(updateDiamonds==true){
		     GameObject.Find ("Diamonds").GetComponent<UILabel> ().text = "Diamonds : "+( PlayerPrefs.GetInt ("diamonds")).ToString();
			 updateDiamonds=false;
		}

	}

}
