﻿using UnityEngine;
using System.Collections;
using System.Linq; 
using System;
using System.IO;

public class Magnet : CSVReader{
	public string imageName="Bright";
	// Use this for initialization
	public int[] magnet_Costs= new int[6];
	public int    isMagetLock=0;
	public int     magnet_Level;
	public float[] magnet_times = new float[5];

	Magnet()
	{
		isMagetLock = PlayerPrefs.GetInt ("isMagnetActive");
		magnet_Level= PlayerPrefs.GetInt ("magnetLevel");
		}

	static Magnet myInstance = null;
	
	public static Magnet getInstance(){
		if (myInstance == null)
			myInstance = new Magnet ();
		return myInstance;
	}
	public void magnetForStore()
	{
		isMagetLock = PlayerPrefs.GetInt ("isMagnetActive");
		magnet_Level= PlayerPrefs.GetInt ("magnetLevel");
	}

	public void magnetLock(){
		
		//isMagetLock = PlayerPrefs.GetInt ("isMagnetActive");
		if(isMagetLock==0)
		{
			isMagetLock=1;
			
			PlayerPrefs.SetInt ("isMagnetActive", isMagetLock);
			Debug.Log ("Magnet is Active=");
		
		}
		else {Debug.Log ("Magnet is already Active");
		}
	}


	public void set_ScaleCost_FomeFile()
	{
		TextAsset text=(TextAsset)Resources.Load("StoneData", typeof(TextAsset));
		
		String[,] fileLines = base.SplitCsvGrid(text.ToString());
		
		//int[] row = new int[5];
		
		for (int i=0; i<5; i++) 
		{
			int.TryParse(fileLines[i+1,4], out magnet_Costs[i]);
		//	Debug.Log(magnet_Costs[i]);
		}
		
	}
	

	public void set_ScaleTimes_FomeFile()
	{

		set_ScaleCost_FomeFile ();
		TextAsset text=(TextAsset)Resources.Load("StoneData", typeof(TextAsset));
		
		String[,] fileLines = base.SplitCsvGrid(text.ToString());
		
		for (int i=0; i<5; i++) 
		{
			float.TryParse(fileLines[i+1,5], out magnet_times[i]);
			//Debug.Log(magnet_times[i]);
		}
		
	}


	public int magnet_Level_cost(){
		//int magnet_Cost=0;
	
			set_ScaleCost_FomeFile ();
			int	magnet_Cost = magnet_Costs [magnet_Level];
			return magnet_Cost;
	}



	public float magnet_Level_TimeUp()
	{
		set_ScaleTimes_FomeFile ();
			float magnet_time=0;
			if(isMagetLock==1)
			{
				magnet_time = magnet_times [magnet_Level-1];
			}
			return magnet_time;
		
	}
	public void MagnetLevelUps()
	{
		if(magnet_Level< 5)
		{
//			Debug.Log("Greater");
			GameObject.Find("MagnetLevel"+(magnet_Level)).GetComponent<UISprite>().spriteName=imageName;
		}
		magnet_Level++;
		if (magnet_Level > 5) {
		//	GameObject.Find ("MagnetLabel").GetComponent<UILabel> ().text ="Fully Upgreated";
			GameObject.Find ("MagnetInst").GetComponent<UILabel> ().text ="Fully Upgreated";
			magnet_Level = 5;
			}

		PlayerPrefs.SetInt ("magnetLevel",magnet_Level);
//		Debug.Log ("magnetLevel No=" +magnet_Level);
	}

//	static public string[,] SplitCsvGrid(string csvText)
//	{
//		string[] lines = csvText.Split("\n"[0]); 
//		
//		// finds the max width of row
//		int width = 0; 
//		for (int i = 0; i < lines.Length; i++)
//		{
//			string[] row = SplitCsvLine( lines[i] ); 
//			width = Mathf.Max(width, row.Length); 
//		}
//		
//		// creates new 2D string grid to output to
//		string[,] outputGrid = new string[width + 1, lines.Length + 1]; 
//		for (int y = 0; y < lines.Length; y++)
//		{
//			string[] row = SplitCsvLine( lines[y] ); 
//			for (int x = 0; x < row.Length; x++) 
//			{
//				outputGrid[x,y] = row[x]; 
//				
//				// This line was to replace "" with " in my output. 
//				// Include or edit it as you wish.
//				outputGrid[x,y] = outputGrid[x,y].Replace("\"\"", "\"");
//			}
//		}
//		
//		return outputGrid; 
//	}
//	
//	// splits a CSV row 
//	static public string[] SplitCsvLine(string line)
//	{
//		return (from System.Text.RegularExpressions.Match m in System.Text.RegularExpressions.Regex.Matches(line,
//		                                                                                                    @"(((?<x>(?=[,\r\n]+))|""(?<x>([^""]|"""")+)""|(?<x>[^,\r\n]+)),?)", 
//		                                                                                                    System.Text.RegularExpressions.RegexOptions.ExplicitCapture)
//		        select m.Groups[1].Value).ToArray();
//	}
//	//////////

}
