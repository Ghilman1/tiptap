﻿using UnityEngine;
using System.Collections;
using System.Linq; 
using System;
using System.IO;

public class StoneData : CSVReader {

	public int Marbl_Cost;
	public int MetalChrome_Cost;
	public int MetalCopper_Cost;

	StoneData(){}

	static StoneData myInstance = null;
	
	public static StoneData getInstance(){
		if (myInstance == null)
			myInstance = new StoneData ();
		return myInstance;
	}

	public void set_MarblCost_FomeFile()
	{
		TextAsset text=(TextAsset)Resources.Load("StoneData", typeof(TextAsset));
		
		String[,] fileLines = base.SplitCsvGrid(text.ToString());
		int.TryParse(fileLines[1,14], out Marbl_Cost);
	//	Debug.Log(Marbl_Cost);

		
	}
	public void set_MetalChromeCost_FomeFile()
	{
		TextAsset text=(TextAsset)Resources.Load("StoneData", typeof(TextAsset));
		
        String[,] fileLines = base.SplitCsvGrid(text.ToString());
		int.TryParse(fileLines[1,15], out MetalChrome_Cost);
	//	Debug.Log(MetalChrome_Cost);
		
	}
	public void set_MetalCopperCost_FomeFile()
	{
		TextAsset text=(TextAsset)Resources.Load("StoneData", typeof(TextAsset));
		
		String[,] fileLines = base.SplitCsvGrid(text.ToString());
	    int.TryParse(fileLines[1,16], out MetalCopper_Cost);
	//	Debug.Log(MetalCopper_Cost);

		
	}

	public int marble_cost(){
		//int magic_Cost = 0;
		
		set_MarblCost_FomeFile ();
		int	marblecost = Marbl_Cost;
		return marblecost;
	}
	public int metalChrome_Cost(){
		//int magic_Cost = 0;
		
		set_MetalChromeCost_FomeFile ();
		int	metalChromeCost = MetalChrome_Cost;
		return metalChromeCost;
	}
	public int metalCopper_Cost(){
		//int magic_Cost = 0;
		
		set_MetalCopperCost_FomeFile ();
		int	metalCopperCost = MetalCopper_Cost;
		return metalCopperCost;
	}


}
