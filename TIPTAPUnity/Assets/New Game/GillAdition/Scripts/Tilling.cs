﻿using UnityEngine;
using System.Collections;

public class Tilling : MonoBehaviour {
//	public Texture texture_Xaxis;
//	public Texture texture_Zaxis;
	public Renderer rend,child0_rend,child1_rend,child2_rend,child3_rend;
	public Material Along_Zaxis,Along_Xaxis;
	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer>();
		child0_rend = this.transform.GetChild (0).GetComponent<Renderer> ();
		child1_rend = this.transform.GetChild (1).GetComponent<Renderer> ();
		child2_rend = this.transform.GetChild (2).GetComponent<Renderer> ();
		//child3_rend = this.transform.GetChild (3).GetComponent<Renderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.localScale.x > 1) {
			child2_rend.material.mainTextureScale =new Vector2(transform.localScale.x, transform.localScale.y);
			//child1_rend.material.mainTextureScale =new Vector2(transform.localScale.y,transform.localScale.x);
		//	rend.material.mainTexture=texture_Xaxis;
			rend.material=Along_Xaxis;
		   rend.material.mainTextureScale =new Vector2(transform.localScale.x,transform.localScale.y);

			
		}else if (transform.localScale.z > 1) {
		
			child0_rend.material.mainTextureScale =new Vector2(transform.localScale.z, 1);
			child1_rend.material.mainTextureScale =new Vector2(transform.localScale.z, 1);
		//	rend.material.mainTexture=texture_Zaxis;
			rend.material=Along_Zaxis;
			rend.material.mainTextureScale =new Vector2( 1,transform.localScale.z );
			
			
		}
	}
}
