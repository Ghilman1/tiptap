﻿using UnityEngine;
using System.Collections;
using System.Linq; 
using System;
using System.IO;

public class SaveMe : CSVReader {

	
	public int[] SaveMe_Costs= new int[5];
	public int saveMeCount=0;


	SaveMe(){

		}


	static SaveMe myInstance = null;
	
	public static SaveMe getInstance(){
		if (myInstance == null)
			myInstance = new SaveMe ();
		return myInstance;
	}

	public void set_SaveMeCost_FomeFile(){
		TextAsset text=(TextAsset)Resources.Load("StoneData", typeof(TextAsset));
		
		String[,] fileLines = base.SplitCsvGrid(text.ToString());
		
		for (int i=0; i<=4; i++) {
			int.TryParse(fileLines[i+1,6], out SaveMe_Costs[i]);

		}
		
	}

	public int saveMeCount_Costs(){
		set_SaveMeCost_FomeFile ();
		int saveMe_cost = SaveMe_Costs [saveMeCount];
		return saveMe_cost;
	}


}
