﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Analytics;
using Debug = UnityEngine.Debug;

public class FlurryUpdater : MonoBehaviour {
	
	public static string deviceID;
	public static int numberOf_Tries=0;
	public static int saveMeUsed_Count=0;
	public static int score;
	public static int avg_Score=0;
	public static int BallScaleUpLevel,MagnetLevel,MagicLevel;



//	public static int level_2_Tries=0;
//	public static int level_3_Tries=0;

	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(this.gameObject);
		#if UNITY_ANDROID && !UNITY_EDITOR

		UnityEngine.AndroidJavaClass up = new UnityEngine.AndroidJavaClass("com.unity3d.player.UnityPlayer");
		UnityEngine.AndroidJavaObject currentActivity = up.GetStatic<UnityEngine.AndroidJavaObject>("currentActivity");
		UnityEngine.AndroidJavaObject contentResolver = currentActivity.Call<UnityEngine.AndroidJavaObject>("getContentResolver");
		UnityEngine.AndroidJavaObject secure = new UnityEngine.AndroidJavaObject("android.provider.Settings$Secure");
		deviceID = secure.CallStatic<string>("getString", contentResolver, "android_id");

		#endif
		#if UNITY_IOS && !UNITY_EDITOR

		#endif

		numberOf_Tries = numberOf_Tries + 1;

		score = score + (PlayerPrefs.GetInt("distance") + PlayerPrefs.GetInt ("CurrentDiamonds"));
 
         Flurry.Instance.StartSession("ios_api_key", "Android_api_Key");

		#if UNITY_ANDROID && !UNITY_EDITOR
		Flurry.Instance.LogUserID(deviceID);
		#endif

		//Flurry.Instance.EndLogEvent;
	

//		beComeCheater= PlayerPrefs.GetInt("beComeCheater");

//		

	
	}
	public static void updateFlurry()
	{

		avg_Score = score / numberOf_Tries;
	
		BallScaleUpLevel = BallScale.getInstance ().ballScale_Level;

		MagicLevel = Magic.getInstance ().magic_Level;

		MagnetLevel= Magnet.getInstance ().magnet_Level;
		
				Flurry.Instance.LogEvent ("Roll The Ball", new Dictionary<string, string>
				                          {
					
			        { "Number Of Tries", (numberOf_Tries).ToString("F2")},
			        { "Save Me Used", (saveMeUsed_Count).ToString("F2") },
			        { "Average Score", (avg_Score).ToString("F2")},
			        { "Magic Level", (MagicLevel).ToString("F2")},
			        { "Magnet Level", (MagnetLevel).ToString("F2")},
			        { "BallScaleUp Level", (BallScaleUpLevel).ToString("F2")}
					
				});
		Debug.Log("numberOf_Tries="+numberOf_Tries);
		Debug.Log ("SaveMeUsed="+saveMeUsed_Count);
		Debug.Log ("score="+score);
		Debug.Log ("avg_Score="+avg_Score);
		Debug.Log ("BallScaleUp Level="+BallScaleUpLevel);
		Debug.Log ("Magic Level" +MagicLevel );
		Debug.Log ("Magnet Level" +MagnetLevel);
		
		

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
