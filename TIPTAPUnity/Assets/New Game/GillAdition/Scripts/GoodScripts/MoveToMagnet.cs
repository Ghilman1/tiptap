﻿using UnityEngine;
using System.Collections;

public class MoveToMagnet : MonoBehaviour {

	public float distance;
	GameObject target;
	public float movingSpeed=2.5f;
	// Use this for initialization
	void Start () {
		target = GameObject.Find ("Sphere") as GameObject;
	
	}
	
	// Update is called once per frame
	void Update () {
		distance=Vector3.Distance(transform.position,target.transform.position);
		
		if (GetMagnet.magnetOn && distance<1.5f) {
			
			transform.position=Vector3.MoveTowards(transform.position,target.transform.position,movingSpeed*Time.deltaTime);

	
	}
}
}
