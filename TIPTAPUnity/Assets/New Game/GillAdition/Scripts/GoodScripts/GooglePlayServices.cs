﻿using UnityEngine;
using System.Collections;

using UnionAssets.FLE;
using System.Collections.Generic;

//using GooglePlayGames;
//using UnityEngine.SocialPlatforms;

public class GooglePlayServices : MonoBehaviour {
	private const string LEADERBOARD_NAME = "leaderboard_best_scores";
	//example
	private const string LEADERBOARD_ID = "CgkIgqncu-kSEAIQAQ";
	//private const string LEADERBOARD_ID = "REPLACE_WITH_YOUR_ID";
	private const string INCREMENTAL_ACHIEVEMENT_ID = "CgkIgqncu-kSEAIQAg";
	//private const string INCREMENTAL_ACHIEVEMENT_ID = "REPLACE_WITH_YOUR_ID";
	// Use this for initialization

	public SA_Label playerLabel;
	
	public SA_Label a_id;
	public SA_Label a_name;
	public SA_Label a_descr;
	public SA_Label a_type;
	public SA_Label a_state;
	public SA_Label a_steps;
	public SA_Label a_total;
	
	
	public SA_Label b_id;
	public SA_Label b_name;
	public SA_Label b_all_time;

	int isConnected=0; 
	//public static bool isGPS_Call=false;
	//public static bool isLeaderBoard_Call=false;
//	public static bool isAchievments_Call=false;
//	public static bool isUnlookAchievement=false;
//	public static bool isScoreUpdate=false;

	WWW www;
	//private bool isConnectedToInt=false;
	//public static bool isResetAchiv=false,isUnsend=false;

	void Start () {

//		isGPS_Call = false;
	//	isLeaderBoard_Call=false;
	//	isAchievments_Call=false;
	//	isUnlookAchievement=false;
	//	isScoreUpdate=false;
	//	isConnectedToInt=false;
		www = new WWW("http://google.com");
//		playerLabel.text = "Player Disconnected";
//		defaulttexture = avatar.GetComponent<Renderer>().material.mainTexture;
		
		//listen for GooglePlayConnection events
		GooglePlayConnection.instance.addEventListener (GooglePlayConnection.PLAYER_CONNECTED, OnPlayerConnected);
		GooglePlayConnection.instance.addEventListener (GooglePlayConnection.PLAYER_DISCONNECTED, OnPlayerDisconnected);
		
		
		GooglePlayConnection.ActionConnectionResultReceived += ActionConnectionResultReceived;
		
		
		
		//listen for GooglePlayManager events
		GooglePlayManager.instance.addEventListener (GooglePlayManager.ACHIEVEMENT_UPDATED, OnAchievementUpdated);

		GooglePlayManager.instance.addEventListener (GooglePlayManager.SCORE_SUBMITED, OnScoreSubmited);

		GooglePlayManager.instance.addEventListener (GooglePlayManager.SCORE_REQUEST_RECEIVED, OnScoreUpdated);
		
		
		
//		
//		GooglePlayManager.ActionSendGiftResultReceived += OnGiftResult;
//		GooglePlayManager.ActionPendingGameRequestsDetected += OnPendingGiftsDetected;
//		GooglePlayManager.ActionGameRequestsAccepted += OnGameRequestAccepted;
		
		
		GooglePlayManager.ActionOAuthTokenLoaded += ActionOAuthTokenLoaded;
		GooglePlayManager.ActionAvailableDeviceAccountsLoaded += ActionAvailableDeviceAccountsLoaded;
		
		GooglePlayManager.instance.addEventListener (GooglePlayManager.ACHIEVEMENTS_LOADED, OnAchievmnetsLoadedInfoListner);
		
		
		if(GooglePlayConnection.state == GPConnectionState.STATE_CONNECTED) {
			//checking if player already connected
			OnPlayerConnected ();
		} 




		if (PlayerPrefs.GetInt ("isConnected") == 0) {

			ConncetButtonPress();
			PlayerPrefs.SetInt ("isConnected",1);
		}

		}

	
	// Update is called once per frame
	void Update () {
//		if(isUnsend){
//			Debug.Log ("Number of unsend Achivivements="+PlayerPrefs.GetInt("countNotSendAchiv"));
//			AndroidMessage.Create("Number of unsend Achivivements", "Id:"+PlayerPrefs.GetInt("countNotSendAchiv"));
//		    isUnsend=false;	
//		}
//		if(isResetAchiv){
//			GooglePlayManager.instance.ResetAllAchievements();
//			PlayerPrefs.SetInt ("rollingAchievementCount",0);
//			PlayerPrefs.SetInt("countNotSendAchiv",0);
//			isResetAchiv=false;
//		}
//		if(isGPS_Call){
//			ConncetButtonPress ();
//			isGPS_Call=false;
//		}
//		if(isLeaderBoard_Call){
//			showLeaderBoardsUI ();
//			isLeaderBoard_Call=false;
//		}
//		if (isAchievments_Call) {
//			showAchievementsUI();
//			isAchievments_Call=false;
//				}
//		if(isUnlookAchievement){
//			StartCoroutine(checkConniction());
//		    isUnlookAchievement=false;
//			}
//		if (isScoreUpdate) {
//			OnScoreUpdated();
//			isScoreUpdate=false;
//				}
	
	}

	/// <Temp>
	public void resetAchievments(){
	//	Debug.Log ( "Reset ..... Clicked");
		GooglePlayManager.instance.ResetAllAchievements();
		PlayerPrefs.SetInt ("rollingAchievementCount",0);
		PlayerPrefs.SetInt("countNotSendAchiv",0);
	}
	public void UnsendAchievements(){
		Debug.Log ( "Unsend ..... Clicked");
		Debug.Log ("Number of unsend Achivivements="+PlayerPrefs.GetInt("countNotSendAchiv"));
		AndroidMessage.Create("Number of unsend Achivivements", "Id:"+PlayerPrefs.GetInt("countNotSendAchiv"));
	}
	//////
	public void scoreUpdate(){
		OnScoreUpdated();
		}
	public void unlookAchievement(){
		StartCoroutine(checkConniction());
		}
	public void showLeaderBoardsUI() {
		Debug.Log ("LeaderBoardsUI");
		GooglePlayManager.instance.ShowLeaderBoardsUI ();
		//SA_StatusBar.text = "Showing Leader Boards UI";
	}
	public void showAchievementsUI() {
		Debug.Log ("AchievementsUI");
		GooglePlayManager.instance.ShowAchievementsUI ();
	//	SA_StatusBar.text = "Showing Achievements UI";
		}
	public void ConncetButtonPress() {
	    Debug.Log("GooglePlayManager State  -> " + GooglePlayConnection.state.ToString());
		if(GooglePlayConnection.state == GPConnectionState.STATE_CONNECTED) {
		//	SA_StatusBar.text = "Disconnecting from Play Service...";
			GooglePlayConnection.instance.disconnect ();
		} else {
		//	SA_StatusBar.text = "Connecting to Play Service...";
			GooglePlayConnection.instance.connect ();
		}
	}
	IEnumerator  checkConniction()
	{
		yield return www;
	
		if(www.error != null)
		{
			Debug.Log("faild to connect to internet, trying after 2 seconds. hhhhh");
			notConnectedToNet();
		}else
		{
			if(GooglePlayConnection.state == GPConnectionState.STATE_CONNECTED) {
			Debug.Log("connected to internet..hhhhh");
			GooglePlayManager.instance.UnlockAchievementById(PlayerPrefs.GetString("AchievementID"));
			GooglePlayManager.ActionAchievementUpdated += OnAchievementUpdate;
			}else {
				notConnectedToNet();
				ConncetButtonPress();
			}
		}
		
	}
	public void notConnectedToNet(){
		int countNotSendAchiv=PlayerPrefs.GetInt("countNotSendAchiv");
		countNotSendAchiv = countNotSendAchiv + 1;
		string ID=PlayerPrefs.GetString ("AchievementID");
		PlayerPrefs.SetString ("ID_NotSendAchiv"+countNotSendAchiv,ID);
		PlayerPrefs.SetInt ("countNotSendAchiv",countNotSendAchiv);
	}

	private void OnAchievementUpdate(GP_GamesResult result) {
		if(result.isFailure){
		//AndroidMessage.Create("Achievement Updated statement ", "Id: " +achievementId + "\n status: " + responseCode);
		}
		if (result.isSuccess) {
		//    AndroidMessage.Create("Achievement Updated statement ", "Id: " +achievementId + "\n status: " + responseCode);
			if(1<=PlayerPrefs.GetInt("countNotSendAchiv")){
				int tempNumber =PlayerPrefs.GetInt("countNotSendAchiv");
				GooglePlayManager.instance.UnlockAchievementById(PlayerPrefs.GetString("ID_NotSendAchiv"+tempNumber));
			    tempNumber=tempNumber-1;
				PlayerPrefs.SetInt("countNotSendAchiv",tempNumber);
			}
			
		}
		//string negativeStatement = result.isFailure.ToString();
	//	string PositiveStatement = result.isSuccess.ToString ();

	//	Debug.Log("Achievement Updated  Id: " +achievementId + "\n status: " + responseCode);
	//	AndroidNative.showMessage ("Achievement Updated ", "Id: " +achievementId + "\n status: " + responseCode);
	}





	
	private void reportAchievement() {
		GooglePlayManager.instance.UnlockAchievement ("achievement_simple_achievement_example");
		SA_StatusBar.text = "Reporting achievement_prime...";
	}


	private void loadLeaderBoards() {
		
		//listening for load event 
		GooglePlayManager.instance.addEventListener (GooglePlayManager.LEADERBOARDS_LOADED, OnLeaderBoardsLoaded);
		GooglePlayManager.instance.LoadLeaderBoards ();
		SA_StatusBar.text = "Loading Leader Boards Data...";
	}

	
	private void showLeaderBoard() {
		GooglePlayManager.instance.ShowLeaderBoardById (LEADERBOARD_ID);
		SA_StatusBar.text = "Shwoing Leader Board UI for " + LEADERBOARD_ID;
	}


	
	private void OnPlayerDisconnected() {
		SA_StatusBar.text = "Player Disconnected";
		playerLabel.text = "Player Disconnected";
	}
	
	private void OnPlayerConnected() {
		SA_StatusBar.text = "Player Connected";
		playerLabel.text = GooglePlayManager.instance.player.name + "(" + GooglePlayManager.instance.currentAccount + ")";
	}
	private void ActionConnectionResultReceived(GooglePlayConnectionResult result) {
		
		if(result.IsSuccess) {
			Debug.Log("Connected!");
		} else {
			Debug.Log("Cnnection failed with code: " + result.code.ToString());
		}
		SA_StatusBar.text = "ConnectionResul:  " + result.code.ToString();
	}

	private void OnAchievementUpdated(CEvent e) {
		GP_GamesResult result = e.data as GP_GamesResult;
		SA_StatusBar.text = "Achievment Updated: Id: " + result.achievementId + "\n status: " + result.message;
		AN_PoupsProxy.showMessage ("Achievment Updated ", "Id: " + result.achievementId + "\n status: " + result.message);
	}
	private void OnScoreSubmited(CEvent e) {
		GooglePlayResult result = e.data as GooglePlayResult;

		SA_StatusBar.text = "Score Submited:  " + result.message;
	}

	private void OnScoreUpdated() {
		if (GooglePlayConnection.state == GPConnectionState.STATE_CONNECTED) {
						int score = PlayerPrefs.GetInt ("PlayerScore");
						GooglePlayManager.instance.submitScoreById (LEADERBOARD_ID, score);
				}
		//UpdateBoardInfo();
	}

	
	private void OnLeaderBoardsLoaded(CEvent e) {
		GooglePlayManager.instance.removeEventListener (GooglePlayManager.LEADERBOARDS_LOADED, OnLeaderBoardsLoaded);
		
		GooglePlayResult result = e.data as GooglePlayResult;
		if(result.isSuccess) {
			if( GooglePlayManager.instance.GetLeaderBoard(LEADERBOARD_ID) == null) {
				AN_PoupsProxy.showMessage("Leader boards loaded", LEADERBOARD_ID + " not found in leader boards list");
				return;
			}
			
			
			SA_StatusBar.text = LEADERBOARD_NAME + "  score  " + GooglePlayManager.instance.GetLeaderBoard(LEADERBOARD_ID).GetCurrentPlayerScore(GPBoardTimeSpan.ALL_TIME, GPCollectionType.FRIENDS).score.ToString();
			AN_PoupsProxy.showMessage (LEADERBOARD_NAME + "  score",  GooglePlayManager.instance.GetLeaderBoard(LEADERBOARD_ID).GetCurrentPlayerScore(GPBoardTimeSpan.ALL_TIME, GPCollectionType.FRIENDS).score.ToString());
		} else {
			SA_StatusBar.text = result.message;
			AN_PoupsProxy.showMessage ("Leader-Boards Loaded error: ", result.message);
		}
		
		UpdateBoardInfo();
		
	}
	private void UpdateBoardInfo() {
		GPLeaderBoard leaderboard = GooglePlayManager.instance.GetLeaderBoard(LEADERBOARD_ID);
		if(leaderboard != null) {
			b_id.text 		= "Id: " + leaderboard.id;
			b_name.text 	= "Name: " +leaderboard.name;
			b_all_time.text = "All Time Score: " + leaderboard.GetCurrentPlayerScore(GPBoardTimeSpan.ALL_TIME, GPCollectionType.FRIENDS).score;
			
		} else {
			b_all_time.text = "All Time Score: " + " -1";
		}
	}

	
	private void ActionAvailableDeviceAccountsLoaded(List<string> accounts) {
		string msg = "Device contains following google accounts:" + "\n";
		foreach(string acc in GooglePlayManager.instance.deviceGoogleAccountList) {
			msg += acc + "\n";
		} 
		
		AndroidDialog dialog = AndroidDialog.Create("Accounts Loaded", msg, "Sign With Fitst one", "Do Nothing");
	    dialog.OnComplete += SighDialogComplete;
		
	}
	private void SighDialogComplete (AndroidDialogResult res) {
		if(res == AndroidDialogResult.YES) {
			GooglePlayConnection.instance.connect(GooglePlayManager.instance.deviceGoogleAccountList[0]);
		}
		
	}
//
	private void ActionOAuthTokenLoaded(string token) {
		
		AN_PoupsProxy.showMessage("Token Loaded", GooglePlayManager.instance.loadedAuthToken);
	}
	private void OnAchievmnetsLoadedInfoListner() {
		GPAchievement achievement = GooglePlayManager.instance.GetAchievement(INCREMENTAL_ACHIEVEMENT_ID);
		
		
		if(achievement != null) {
			a_id.text 		= "Id: " + achievement.id;
			a_name.text 	= "Name: " +achievement.name;
			a_descr.text 	= "Description: " + achievement.description;
			a_type.text 	= "Type: " + achievement.type.ToString();
			a_state.text 	= "State: " + achievement.state.ToString();
			a_steps.text 	= "CurrentSteps: " + achievement.currentSteps.ToString();
			a_total.text 	= "TotalSteps: " + achievement.totalSteps.ToString();
		}
	}


}
