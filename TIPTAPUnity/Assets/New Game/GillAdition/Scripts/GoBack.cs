﻿using UnityEngine;
using System.Collections;

public class GoBack : MonoBehaviour {
	public GameObject tiled,clone,Game,ApplicationQuit;
	public static bool isRestart;
	public static int isUpgraders,isMenu,isGooglePlayServ,isStats,isSettings,isHome,isGame,isObjectives,isGameEnd,isStore,isFaceBook;
	public GameObject upgraders,GooglePlayServ,stats,settings,Objectives,_Store,FbCall,menu,home;
	// Use this for initialization
	public static bool isXexisSave=false,isZexisSave=false;
	public static bool isBackButton=false;
	void Start () {

//		GameObject.Find ("Sphere").GetComponent<Player_ZigZag> ().enabled = false;
//		GameObject.Find ("Sphere").GetComponent<Rigidbody>().isKinematic=true;
		isBackButton = false;
		isUpgraders = 0;
		isMenu = 0;
		isGooglePlayServ = 0;
		isStats = 0;
		isSettings = 0;
		isHome = 1;
		isGame = 0;
		isObjectives = 0;
		isGameEnd = 0;
		isStore = 0;
		isFaceBook = 0;
	
	//	DontDestroyOnLoad(transform.gameObject);
	
	}

	public static void sphereMove (){
		GameObject.Find ("Sphere").GetComponent<Player_ZigZag> ().enabled = true;
		GameObject.Find ("Sphere").GetComponent<Rigidbody>().isKinematic=false;
		GameObject.Find ("Spawner").GetComponent<ZigZagTileSpawner>().enabled=true;
	}
	
	// Update is called once per frame
	void Update () {
		
		if (isUpgraders==1){
			upgraders.SetActive(true);
		}else{
			upgraders.SetActive(false);
			UpgradersNgui.IsNotEnoughRes=false;
		//	isUpgraders=2;
		} 
		
		if (isMenu==1){
			menu.SetActive(true);
		} else{menu.SetActive(false);
		} 
		
		if (isGooglePlayServ==1){
			GooglePlayServ.SetActive(true);
		} else {GooglePlayServ.SetActive(false);
		} 
		
		if (isStats==1){
			stats.SetActive(true);
		} else{stats.SetActive(false);
		} 
		if (isSettings==1){
			settings.SetActive(true);
		} else {settings.SetActive(false);
		} 
		if (isHome==1){
			home.SetActive(true);
		} else{home.SetActive(false);
		} 
		
		if (isGame==1){
			Game.SetActive(true);
		} else {Game.SetActive(false);
		} 
		
		if (isSettings==1){
			settings.SetActive(true);
		} else{settings.SetActive(false);
		} 
		
		if (isObjectives==1){
			Objectives.SetActive(true);
		} else{
			Objectives.SetActive(false);
			GameObject.Find ("Achievements").GetComponent<AchievementsNgui>().AchivRewardCollect.SetActive(false);
			//AchievementsNgui.isAchivRewardCollect=false;
		} 
		
		if (isStore==1){
			_Store.SetActive(true);
		} else {_Store.SetActive(false);
			StoreNguiUpdater.IsMaterialBuyed=false;
		} 
		if (isFaceBook==1){
			FbCall.SetActive(true);
		} else {FbCall.SetActive(false);
		} 
		
		

		if (isRestart) {
			Vector3 offset=new Vector3(0.0f,0.8f,0.5f);
			clone = Instantiate (tiled, offset, Quaternion.identity) as GameObject;
			isRestart=false;
				}
		if ((Input.GetKeyDown (KeyCode.Escape))||isBackButton) { 
			if(isMenu == 0&& (isFaceBook==1||isGooglePlayServ==1||isSettings==1||isStats==1||isUpgraders==1||isObjectives==1||isStore==1)){
//				upgraders.SetActive(false);
//				GooglePlayServ.SetActive(false);
//				stats.SetActive(false);
//				settings.SetActive(false);
//				Objectives.SetActive(false);
//				_Store.SetActive(false);
//				FbCall.SetActive(false);
//
//				menu.SetActive(true);
				
				isMenu=1;
				
				isObjectives=0;
				isUpgraders=0;
				isGooglePlayServ=0;
				isSettings=0;
				isStats=0;
				isStore=0;
				isFaceBook=0;
				
				
			}else if(GoBack.isMenu== 1 &&GoBack.isHome== 0 ){
				//home.SetActive(true);
				//menu.SetActive(false);
				
				GoBack.isHome=1;
				GoBack.isMenu=0;
			}else if(GoBack.isGame==1 &&(GoBack.isMenu== 0 || GoBack.isHome== 0) ){
				Application.LoadLevel(1);
			}else if(isHome==1&&isMenu==0){ApplicationQuit.SetActive(true);}
			
			isBackButton = false;
		}
	
	}
}
