﻿using UnityEngine;
using System.Collections;

public class StoreNguiUpdater : MonoBehaviour {

	public static bool _isChangeToChrome=false;
	public static bool _isChangeToMarble=false,_isChangeToCupper=false;
	// Use this for initialization
	private UISprite newSprite1,newSprite2,newSprite3;
	public GameObject MarbleButton,CupperButon,ChromeButton;
	public Renderer rand;
	public Material Marble,Cupper,Chrome,M_Default;
	public GameObject sphere; 
	public static bool IsMaterialBuyed=false;
	public GameObject materialBuyeds;
	void Start () {
		_isChangeToMarble = false;
		_isChangeToCupper = false;
		_isChangeToMarble = false;
		newSprite1 = MarbleButton.GetComponent<UISprite> ();
	    newSprite2 = CupperButon.GetComponent<UISprite> ();
	    newSprite3 = ChromeButton.GetComponent<UISprite> ();
		rand= sphere.GetComponent<Renderer>();
		
		if (PlayerPrefs.GetInt ("isMarbleMaterial") == 0) {
	        newSprite1.spriteName = "Lock_marble";
		} else if (PlayerPrefs.GetInt ("isMarbleMaterial") == 1) {
	        newSprite1.spriteName = "marble";
		}
		if (PlayerPrefs.GetInt ("isCupperMaterial") == 0) {
			newSprite2.spriteName = "Lock_cupper";
	    } else if (PlayerPrefs.GetInt ("isCupperMaterial") == 1) {
		    newSprite2.spriteName = "cupper";
	    }
		if (PlayerPrefs.GetInt ("isChromeMaterial") == 0) {
			newSprite3.spriteName = "Lock_Chrome";
		} else if (PlayerPrefs.GetInt ("isChromeMaterial") == 1) {
			newSprite3.spriteName = "Chrome";
		}

		if(PlayerPrefs.GetString("Material")=="ChromeMaterial"){
			rand.material=Chrome;
		}else if(PlayerPrefs.GetString("Material")=="CupperMaterial"){
			rand.material=Cupper;
		}else if(PlayerPrefs.GetString("Material")=="MarbleMaterial"){
			rand.material=Marble;
		} else if(PlayerPrefs.GetString("Material")=="DefaultMaterial"){
			rand.material=M_Default;
		}
		}



	// Update is called once per frame
	void Update () {
	
		if(_isChangeToChrome){
			UISprite newSprite33 = ChromeButton.GetComponent<UISprite> ();
		
			newSprite33.spriteName="Chrome";
		//	_isChangeToChrome=false;
		}
		if(_isChangeToMarble){
			UISprite newSprite11 = MarbleButton.GetComponent<UISprite> ();
			
			newSprite11.spriteName="marble";
		 //   _isChangeToMarble=false;
		}
		if(_isChangeToCupper){
			UISprite newSprite22 = CupperButon.GetComponent<UISprite> ();
            newSprite22.spriteName="cupper";
		//    _isChangeToCupper=false;
		}

		if (IsMaterialBuyed == true) {materialBuyeds.SetActive(true);} 
		else {materialBuyeds.SetActive(false);}

	}
	public static void chromeMaterial(){
		PlayerPrefs.SetString ("Material", "ChromeMaterial");
	}
	public static void cupperMaterial(){
		PlayerPrefs.SetString ("Material","CupperMaterial");
	}
	public static void marbleMaterial(){
		PlayerPrefs.SetString ("Material","MarbleMaterial");
	}
	public static void defaultMaterial(){
		PlayerPrefs.SetString ("Material", "DefaultMaterial");
	}
}
