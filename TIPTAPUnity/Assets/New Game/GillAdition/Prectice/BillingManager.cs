﻿using UnityEngine;
using System.Collections;
using UnionAssets.FLE;
public class BillingManager : MonoBehaviour {
	
	
	private static bool _isInited = false;
	
	public const string REMOVE_ADS =  "remove_ads";
	//public const string PACK1 = "pack_1";
	//public const string PACK2 = "pack_2";
	
	public const string base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAg1HzhZZWOE/Uo8J3HbW0eFk/RpFQ4ibeICT48tLyw0/Oh/8vOkp1Vp2tOmoRVeyDNCa0yhZrYvWyRwkNAQ3JDVXf3J2xOK8An1Bo/P1M3lp1XwvNDtIh+GyS1mAYXaQIKjxBjQkpm0BGf9uBnE3GBX3FWjvd7SUEnOInAoAIGmkvjDLYqWJRcPnp0MnUE/UZlqvU+az0U8WYpw/ZdMaMtFb3sEGckWKC7Lj+Ur1r7sxFGQGTlglQCqirL59ECNNJmDBvuoxpTTwXbyM1u7PKBBOVp+iNoC8X4fScbuzS3iEJrp9CplPh1ruGjwhYvW1aDa+b224AXTNi9Ylbosr5/QIDAQAB";
	
	private static bool ListnersAdded = false;

	void Start () {

		init ();
		}
	public static void init(){
		if(ListnersAdded) {
			return;
		}
		AndroidInAppPurchaseManager.instance.addProduct (REMOVE_ADS);
		//AndroidInAppPurchaseManager.instance.addProduct (PACK1);
		//AndroidInAppPurchaseManager.instance.addProduct (PACK2);
		
		AndroidInAppPurchaseManager.instance.addEventListener (AndroidInAppPurchaseManager.ON_PRODUCT_PURCHASED,OnProductPurchased);
		AndroidInAppPurchaseManager.instance.addEventListener (AndroidInAppPurchaseManager.ON_PRODUCT_CONSUMED,  OnProductConsumed);
		
		//initilaizing store
		AndroidInAppPurchaseManager.instance.addEventListener (AndroidInAppPurchaseManager.ON_BILLING_SETUP_FINISHED, OnBillingConnected);
		
		AndroidInAppPurchaseManager.instance.loadStore (base64EncodedPublicKey);
		ListnersAdded = true;
	}
	
	public static void Purchase(string SKU) {
		AndroidInAppPurchaseManager.instance.purchase (SKU);
	}
	
	public static bool isInited {
		get {
			return _isInited;
		}
	}
	
	private static void OnProductPurchased(CEvent e) {
		BillingResult result = e.data as BillingResult;
		if(result.isSuccess) {
			if(result.purchase.SKU == REMOVE_ADS){
			//	GameStore.GetInstance().BuyAds();
			}
			//			else if(result.purchase.SKU == PACK1){
			//				GameStore.GetInstance().BuyPack1();
			//			}else if(result.purchase.SKU == PACK2){
			//
			//			}
		} else {
			AndroidMessage.Create("Product Purchase Failed", result.response.ToString() + " " + result.message);
		}
	}
	
	private static void OnProductConsumed(CEvent e) {
		BillingResult result = e.data as BillingResult;
		
		
		//Debug.Log ("Cousume Responce: " + result.response.ToString() + " " + result.message);
	}
	
	
	private static void OnBillingConnected(CEvent e) {
		BillingResult result = e.data as BillingResult;
		AndroidInAppPurchaseManager.instance.removeEventListener (AndroidInAppPurchaseManager.ON_BILLING_SETUP_FINISHED, OnBillingConnected);
		if(result.isSuccess) {
			//Store connection is Successful. Next we loading product and customer purchasing details
			AndroidInAppPurchaseManager.instance.retrieveProducDetails();
			AndroidInAppPurchaseManager.instance.addEventListener (AndroidInAppPurchaseManager.ON_RETRIEVE_PRODUC_FINISHED, OnRetriveProductsFinised);
			AndroidInAppPurchaseManager.instance.retrieveProducDetails();
		} 
		
			AndroidMessage.Create("Connection Responce", result.response.ToString() + " " + result.message);
		//		Debug.Log ("Connection Responce: " + result.response.ToString() + " " + result.message);
	}
	private static void OnRetriveProductsFinised(CEvent e) {
		BillingResult result = e.data as BillingResult;
		AndroidInAppPurchaseManager.instance.removeEventListener (AndroidInAppPurchaseManager.ON_RETRIEVE_PRODUC_FINISHED, OnRetriveProductsFinised);
		
		if(result.isSuccess) {
			
		} else {
		AndroidMessage.Create("Connection Responce", result.response.ToString() + " " + result.message);
		}
		
	}
	
}
