﻿using UnityEngine;
using System.Collections;
using System.Linq; 



public class TestData1 : MonoBehaviour {

	// Use this for initialization

	public int randomPower;
	// Use this for initialization
	public void rendomPowerUps()
	{
		randomPower = Random.Range (1,4);
		Debug.Log (randomPower);
	}

	void Start () {
	//	TextAsset text=(TextAsset)Resources.Load("StoneData", typeof(TextAsset));
		
//		String[,] fileLines = SplitCsvGrid(text.ToString());
		rendomPowerUps ();

		//			// declare a string variable with a value that represents a valid integer
		//			string sillyMeme = "9001";
		//			
		//			int memeValue;
		//			// attempt to parse the value using the TryParse functionality of the integer type
		//			int.TryParse(sillyMeme, out memeValue);
		//			
		//			Debug.Log(memeValue);//9001
//		float[] row = new float[5];
//		
//		for (int i=0; i<5; i++) 
//		{
//			float.TryParse(fileLines[i+1,3], out row[i]);
//			//int.TryParse(fileLines[i,1], out row[i]);
//			//row[i]=fileLines[i,1];
//			Debug.Log(row[i]);
//		}
//		
		//			TextAsset text=(TextAsset)Resources.Load("MagnetData", typeof(TextAsset));
		//
		//			String[,] fileLines = SplitCsvGrid(text.ToString());
		//
		//			for(int i = 0;i< fileLines.GetLength(0);i++){
		//				for(int j = 0;j<fileLines.GetLength(1);j++){
		//
		//					Debug.Log(fileLines[i,j]);
		//				}
		//			}
	
	

	}

	// Update is called once per frame
	void Update () {
	
	}
	static public string[,] SplitCsvGrid(string csvText)
	{
		string[] lines = csvText.Split("\n"[0]); 
		
		// finds the max width of row
		int width = 0; 
		for (int i = 0; i < lines.Length; i++)
		{
			string[] row = SplitCsvLine( lines[i] ); 
			width = Mathf.Max(width, row.Length); 
		}
		
		// creates new 2D string grid to output to
		string[,] outputGrid = new string[width + 1, lines.Length + 1]; 
		for (int y = 0; y < lines.Length; y++)
		{
			string[] row = SplitCsvLine( lines[y] ); 
			for (int x = 0; x < row.Length; x++) 
			{
				outputGrid[x,y] = row[x]; 
				
				// This line was to replace "" with " in my output. 
				// Include or edit it as you wish.
				outputGrid[x,y] = outputGrid[x,y].Replace("\"\"", "\"");
			}
		}
		
		return outputGrid; 
	}
	
	// splits a CSV row 
	static public string[] SplitCsvLine(string line)
	{
		return (from System.Text.RegularExpressions.Match m in System.Text.RegularExpressions.Regex.Matches(line,
		                                                                                                    @"(((?<x>(?=[,\r\n]+))|""(?<x>([^""]|"""")+)""|(?<x>[^,\r\n]+)),?)", 
		                                                                                                    System.Text.RegularExpressions.RegexOptions.ExplicitCapture)
		        select m.Groups[1].Value).ToArray();
	}
}
