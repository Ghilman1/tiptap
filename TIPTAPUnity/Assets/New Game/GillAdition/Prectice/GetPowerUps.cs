﻿using UnityEngine;
using System.Collections;

public class GetPowerUps : MonoBehaviour {

	public bool counterWatchMagnet = false;
	public static float seconds_Magnet=0;
	public static bool magnetOn=false;



	void Start () {
		magnetOn=false;
		seconds_Magnet =Magnet.getInstance().magnet_Level_TimeUp();
	}
	void Update () {
		if(counterWatchMagnet){
		        seconds_Magnet = seconds_Magnet - Time.deltaTime;
		        if (seconds_Magnet < 0) {
				         seconds_Magnet = 0;
				         counterWatchMagnet=false;
				         magnetOn=false;
				}}
	
	}

	void OnTriggerEnter(Collider other2)
	{

		if (other2.gameObject.tag == "Magnet") {
			seconds_Magnet =Magnet.getInstance().magnet_Level_TimeUp();
			other2.gameObject.SetActive (false);
			counterWatchMagnet=true;
			magnetOn=true;
		}


		
	}


}
