﻿ #pragma strict
 
 // using an image that is an 8x8 grid
 // each image is 0.125 in width and 0.125 in height of the full image
 
 // UVs in this example are given as a Rect
 // uvs : start position X, start position y, width, height
 // start positions are staggered to show different images
 // width and height are set to 0.125 (1/8th square of the full image)
 
 public var uvsFront : Rect = new Rect( 0.0, 1.0, 0.125, 0.125 );
 public var uvsBack : Rect = new Rect( 0.125, 0.875, 0.125, 0.125 );
 public var uvsLeft : Rect = new Rect( 0.25, 0.75, 0.125, 0.125 );
 public var uvsRight : Rect = new Rect( 0.375, 0.625, 0.125, 0.125 );
 public var uvsTop : Rect = new Rect( 0.5, 0.5, 0.125, 0.125 );
 public var uvsBottom : Rect = new Rect( 0.625, 0.375, 0.125, 0.125 );
 
 private var theMesh : Mesh;
 private var theUVs : Vector2[];
 private var xOffset : float = 0.0;
 
 function Start() 
 {
     theMesh = transform.GetComponent(MeshFilter).mesh;
     theUVs = new Vector2[theMesh.uv.Length];
     theUVs = theMesh.uv;
     
     SetUVs();
 }
 
 function Update() 
 {
     // change the UV settings in the Inspector, then click the left mouse button to view
     if ( Input.GetMouseButtonUp(0) )
     {
         SetUVs();
     }
 }
 
 // 2 --- 3
 // |     |
 // |     |
 // 0 --- 1
 
 function SetUVs() 
 {
     // - set UV coordinates -
     
     // FRONT    2    3    0    1
     theUVs[2] = Vector2( uvsFront.x, uvsFront.y );
     theUVs[3] = Vector2( uvsFront.x + uvsFront.width, uvsFront.y );
     theUVs[0] = Vector2( uvsFront.x, uvsFront.y - uvsFront.height );
     theUVs[1] = Vector2( uvsFront.x + uvsFront.width, uvsFront.y - uvsFront.height );
     
     // BACK    6    7   10   11
     theUVs[6] = Vector2( uvsBack.x, uvsBack.y );
     theUVs[7] = Vector2( uvsBack.x + uvsBack.width, uvsBack.y );
     theUVs[10] = Vector2( uvsBack.x, uvsBack.y - uvsBack.height );
     theUVs[11] = Vector2( uvsBack.x + uvsBack.width, uvsBack.y - uvsBack.height );
     
     // LEFT   19   17   16   18
     theUVs[19] = Vector2( uvsLeft.x, uvsLeft.y );
     theUVs[17] = Vector2( uvsLeft.x + uvsLeft.width, uvsLeft.y );
     theUVs[16] = Vector2( uvsLeft.x, uvsLeft.y - uvsLeft.height );
     theUVs[18] = Vector2( uvsLeft.x + uvsLeft.width, uvsLeft.y - uvsLeft.height );
     
     // RIGHT   23   21   20   22
     theUVs[23] = Vector2( uvsRight.x, uvsRight.y );
     theUVs[21] = Vector2( uvsRight.x + uvsRight.width, uvsRight.y );
     theUVs[20] = Vector2( uvsRight.x, uvsRight.y - uvsRight.height );
     theUVs[22] = Vector2( uvsRight.x + uvsRight.width, uvsRight.y - uvsRight.height );
     
     // TOP    4    5    8    9
     theUVs[4] = Vector2( uvsTop.x, uvsTop.y );
     theUVs[5] = Vector2( uvsTop.x + uvsTop.width, uvsTop.y );
     theUVs[8] = Vector2( uvsTop.x, uvsTop.y - uvsTop.height );
     theUVs[9] = Vector2( uvsTop.x + uvsTop.width, uvsTop.y - uvsTop.height );
     
     // BOTTOM   15   13   12   14
     theUVs[15] = Vector2( uvsBottom.x, uvsBottom.y );
     theUVs[13] = Vector2( uvsBottom.x + uvsBottom.width, uvsBottom.y );
     theUVs[12] = Vector2( uvsBottom.x, uvsBottom.y - uvsBottom.height );
     theUVs[14] = Vector2( uvsBottom.x + uvsBottom.width, uvsBottom.y - uvsBottom.height );
      
     // - Assign the mesh its new UVs -
     theMesh.uv = theUVs;
 }