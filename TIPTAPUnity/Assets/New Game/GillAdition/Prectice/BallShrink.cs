﻿using UnityEngine;
using System.Collections;

public class BallShrink : MonoBehaviour {
	
	public float seconds=5;
	public int startCount=0;
	
	
	
	public Vector3 Original_Ball_scale; 
	public Vector3 Target_Ball_scale; 
	
	// Use this for initialization
	void Start () {
		seconds = 5;
		Original_Ball_scale = new Vector3 (0.5f, 0.5f, 0.5f);
		Target_Ball_scale= new Vector3 (0.3f, 0.3f, 0.3f);
		
		
	}
	
	// Update is called once per frame
	void Update () {
		
		
		if (startCount == 1) {
			if (transform.localScale !=Target_Ball_scale)
				transform.localScale = transform.localScale - new Vector3 (0.01f, 0.01f, 0.01f);

			seconds = seconds - Time.deltaTime;
			
			if (seconds < 0) {
				seconds = 5;
				startCount = 0;
			}
			
		} else {
			if(transform.localScale != Original_Ball_scale){
				transform.localScale = transform.localScale + new Vector3 (0.01f, 0.01f, 0.01f);
			}
			
		}
		
		
	}
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "BallShirnk") {
			
			startCount=1;
			
			other.gameObject.SetActive (false);
		}
		
		
		
	}
	
}
