NOTE: 	When running space demo, be sure to add scene1 and Intermission to build settings for it to run properly
		To add a level to the build settings use the menu File->Build Settings...

//============================
Getting Started
//============================

1.	Make sure forward rendering path is selected

2.	Use opengl es 2.0 and zero pixel lights on mobile platforms

3.	Add EchoCoreManager and EchoShaderManager prefabs to the scene

4.	Click the EchoShaderManager object to select lighting options:
	a.	1 point light
	b.	1 directional light
	c.	both

5.	Click the EchoCoreManager object to select EchoFXEvent options:
	a. 	set number of maximum EchoFXEvents
	b. 	set Dynamic Add if you wish the number of events to grow automatically

5.	For optimum results:
	a.	Add EchoComponent -OR- script derived from EchoComponent to all GameObjects
		in the scene (all scripts on GameObjects should extend from EchoComponent, 
		because EchoComponent extends from MonoBehaviour and includes CoreFramework methods.)
	b.	Only use Core Framework Shaders


Class Extension Examples

C#:

// Default Method
class MyGameObjectScript : MonoBehaviour
{
}

// CoreFramework Method
class MyGameObjectScript : EchoComponent
{
}


JS:

// Default Method ( by default in JS the class is not specified )
class MyGameObjectScript
{
}

// CoreFramework Method
class  MyGameObjectScript extends EchoComponent
{
}


EchoComponent Options

All EchoComponent GameObjects in the scene must be active in order for CoreFramework to register them.

The following options will appear in the inspector for all objects that include EchoComponent or scripts derived from EchoComponent:


The EchoComponent options are as follows:

•	Active at Start:	Use this to activate or deactivate the GameObject

•	Renderer Enabled:	Use this to enable the mesh renderer

•	Fix Scale:			Corrects mesh import floating point scale errors to 1,1,1 -OR- corrects manually scaled GameObjects by 
						resizing the mesh and colliders to the current scale and setting the localScale to 1,1,1.  This is essential for batching.

•	Add Children:		Use this to add an EchoComponent to children that do not already have an EchoComponent or derivative attached.


Be sure to check out sample projects in SampleProjects folder

Full reference is available at http://www.echologin.com/coreframework.html

support email  core@echologin.com



